#include <stdx/all.h>



class MsgQueueTool : public Application
{
protected:
	XFile file;
	MsgQueue mq;

public:
	bool main()
	{
		if (GetCmdParamCount() <= 1 || GetCmdParam("--help") || GetCmdParam("?"))
		{
			puts("  parameter list");
			puts("---------------------------");
			puts("  -q : queue name");
			puts("  -e : clean queue");
			puts("---------------------------");
			puts("  -g : pop queue");
			puts("  -d : push file");
			puts("  -s : push string");
			puts("  -b : backup queue");
			puts("  -r : restore queue");
			puts("  -l : query queue depth");
			puts("---------------------------");
			puts("");

			return true;
		}

		const char* qname = GetCmdParam("-q");

		if (qname == NULL || *qname == 0)
		{
			puts("please input queue name use '-q' parameter");

			return false;
		}

		if (!mq.open(qname))
		{
			printf("open queue[%s] failed\n", qname);

			return false;
		}

		if (GetCmdParam("-e"))
		{
			if (mq.clear())
			{
				printf("clear queue[%s] success\n", qname);
			}
			else
			{
				printf("clear queue[%s] failed\n", qname);
			}
		}
		else if (GetCmdParam("-l"))
		{
			int sz = mq.size();
			if (sz >= 0)
			{
				printf("queue[%s] length : %d\n", qname, sz);
			}
			else
			{
				printf("query queue[%s] length failed\n", qname);
			}
		}
		else if (GetCmdParam("-g"))
		{
			SmartBuffer buffer = mq.pop();

			if (buffer.isNull())
			{
				printf("queue[%s] is empty\n", qname);

				return false;
			}

			printf("pop queue[%s] success\n", qname);
			puts("---------------------------------------------------------");
			puts(buffer.str());
			puts("---------------------------------------------------------");
		}
		else if (GetCmdParam("-b"))
		{
			const char* data = GetCmdParam("-b");

			if (data == NULL || *data == 0)
			{
				puts("please input backup filename");

				return false;
			}

			if (mq.save(data))
			{
				printf("backup queue[%s] success\n", qname);
			}
			else
			{
				printf("backup queue[%s] failed\n", qname);
			}
		}
		else if (GetCmdParam("-r"))
		{
			const char* data = GetCmdParam("-r");

			if (data == NULL || *data == 0)
			{
				puts("please input restore filename");

				return false;
			}

			if (mq.load(data))
			{
				printf("restore queue[%s] success\n", qname);
			}
			else
			{
				printf("restore queue[%s] failed\n", qname);
			}
		}
		else if (GetCmdParam("-s"))
		{
			const char* data = GetCmdParam("-s");

			if (data == NULL || *data == 0)
			{
				puts("please input the string");

				return false;
			}

			if (mq.push(data, strlen(data) * sizeof(char) + sizeof(char)))
			{
				printf("push queue[%s] success\n", qname);
			}
			else
			{
				printf("push queue[%s] failed\n", qname);
			}
		}
		else if (GetCmdParam("-d"))
		{
			const char* path = GetCmdParam("-d");

			if (path == NULL || *path == 0)
			{
				puts("please input the filename");

				return false;
			}

			file.open(path, eREAD);

			int sz = file.size();

			if (sz <= 0 || sz >= mq.getMemQueue()->usagesize())
			{
				puts("push file data failed");

				return false;
			}

			SmartBuffer buffer(sz);

			if (file.read(buffer.str(), buffer.size()) <= 0)
			{
				puts("push file data failed");

				return false;
			}

			if (mq.push(buffer.str(), buffer.size()))
			{
				puts("push file data success");
			}
			else
			{
				puts("push file data failed");
			}
		}
		else
		{
			puts("undefined command");
		}

		return true;
	}
};


START_APP(MsgQueueTool);