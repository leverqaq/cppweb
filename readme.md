## 功能说明
1. 作者初衷是编写一个web框架支持C++开发cgi程序，于是cppweb诞生了。
2. 作者希望cppweb是一个大一统的框架，即可用于传统服务端编程也可用于web编程，作者希望http协议能做的更多，框架包括以下两个核心服务：
```
webserver：业务服务容器，通过配置也可升级为服务注册中心与定时任务调度中心。
webrouter：接口路由网关服务，对外提供统一的流量入口，主要负责请求分发以及黑白名称配置。
```
3. cppweb在读数据采用epoll网络模型，以任务队列的方式处理具体请求，回包也在任务队列中处理，理论上cppweb可支持单机10000个以上的并发连接。
4. cppweb易拓展，作者开发Java、Python等模块，用于支持Java、Python等语言开发cgi程序，开发者可以直接使用C/C++、Java、Python等语言进行混合开发。
5. cppweb追求小而巧，对于开源库是拿来即用，源码工程自带zlib、sqlite等源码代码，开发者无需另外下载，再此感谢zlib、sqlite等开源库的作者与开发团队。
6. 基于cppweb的微服务集群框架如下图所示，图中<green>绿色</green>部分包括服务注册中心与业务服务集群由webserver服务构成；图中<red>红色</red>部分包括外部接口网关与内部接口网关由webrouter接口路由网关服务构成。关于cppweb的更多内容请可访问[https://www.winfengtech.com/cppweb](https://www.winfengtech.com/cppweb)查看。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0905/112334_5db2c0e0_392413.png "notefile.png")

## 测试数据
1. cppweb在普通PC机(4核8G)上至少可支持每秒10000笔请求。
2. cppweb在1核1G的低配centos系统上至少支持每秒3000笔请求。
3. 下图是cppweb自身的流量监控数据：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0905/112355_2fce47b9_392413.png "notefile (1).png")

## 安装编译
###### 下面我们以centos与ubuntu系统的安装编译为例，讲解如何编译运行cppweb开发框架。如果系统没有自带openssl开发客户端，需要自行进行安装，ubuntu下可以执行`apt-get install libssl-dev`命令进行安装。
1. 执行以下命令[下载源码](https://gitee.com/xungen/cppweb)，不需要使用root用户安装编译。如果想在windows环境下编译运行cppweb，请直接[下载windows平台的源码压缩包](https://gitee.com/xungen/software/raw/master/source/cppweb.7z)。
```
git clone https://gitee.com/xungen/cppweb.git
```
2. 进入源码目录执行`source configure`命令，为使环境变量在当前会话中生效必须使用`source`命令执行`configure`进行编译配置。命令输出结果如下：
```
initialize configure
---------------------------------------------
1.check openssl success
2.check g++ compiler success
3.check java compiler success
4.create product directory success
5.export environment variable success
---------------------------------------------
>>> initialize build-essential success
```
3. 在源码目录下执行`make`命令，正常情况3~5分钟完成编译。在windows系统中你可以用`mingw`编译器在`git bash`中进行编译。
4. 执行`webserver -init $SOURCE_HOME/webapp/etc`命令初始化配置，命令执行成功后会在`$SOURCE_HOME/webapp/etc`目录下生成以下文件：
```
sqlite.db：基础数据文件
config.lua：启动配置文件
dbconfig.lua：数据库配置文件
mimeconfig.lua：MIME类型映射文件
```
5. 初始化完成后执行`strsvr`命令便可启动webserver服务。一个用户只能部署一个webserver服务容器，你可以将多个工程部署到同一个webserver容器中。
6. 用浏览器打开`http://localhost:8888`地址进入webserver管理中心，如果webserver不是部署在本机需要将地址中的`localhost`替换为webserver所在服务器的IP地址，登陆用户与初始密码都为`system`。

## 启动配置文件`config.lua`说明
1. 以`url@`开头的配置为静态资源文件映射配置。
2. 以`dir@`开头的配置为静态资源目录映射配置。
3. 以`cgi@`开头的配置为动态`cgi`动态库映射配置。
4. 以`exe@`开头的配置为动态`cgi`可执行程序映射配置。
5. 配置文件中`url@index`、`cgi@index`配置项为网站首页配置。
6. 可以通过修改配置文件中的`PORT`配置来修改webserver的监听端口，初始配置为`8888`端口。
7. 修改配置文件后可执行`webserver -r`命令来使配置生效，但修改以下配置需要重启服务才能生效：
```
-- 服务ID(必须大于零，保证每个服务唯一)
ID = 1

-- 服务名称(字母与数字，保证每一类服务名称相同)
NAME = "webserver"

-- 服务目录(默认为当前目录，服务工程与资源文件的根目录)
PATH = "$SOURCE_HOME/webapp"

-- 监听端口(非安全连接监听端口)
PORT = 8888

-- 主机地址(绑定的主机地址)
HOST = "0.0.0.0"

-- SSL监听端口(SSL安全连接监听端口，如果不配置则不启用安全连接)
SSL_PORT = 9999

-- PEM证书文件
CERT_FILEPATH = "$SOURCE_HOME/webapp/etc/cert.crt"

-- PEM私钥文件
PRIKEY_FILEPATH = "$SOURCE_HOME/webapp/etc/cert.key"

-- 序列头(每次启动加一)
SEQUENCE_HEAD = 100

-- 单个日志文件最大大小(KB)
LOGFILE_MAXSIZE = 10000

-- 日志存放目录
LOGFILE_PATH = "$SOURCE_HOME/webapp/log"

-- JAVA类搜索目录或JAR包
JAVA_CLASSPATH = "$SOURCE_HOME/webapp/jar/bin"

-- MIME配置文件路径
MIME_CONFIGFILE_PATH = "$SOURCE_HOME/webapp/etc/mimeconfig.lua"

-- PYTHON配置文件路径
PYTHON_CONFIGFILE_PATH = "$SOURCE_HOME/webapp/pyc/__load__.py"

-- 数据库连接配置文件路径
DATABASE_CONFIGFILE_PATH = "$SOURCE_HOME/webapp/etc/dbconfig.lua"
```