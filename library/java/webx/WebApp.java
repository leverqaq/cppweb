package webx;

import stdx.Utils;
import stdx.Optional;
import stdx.Required;
import stdx.ConfigFile;
import webx.json.JsonObject;
import webx.http.HttpRequest;
import webx.http.HttpResponse;

import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.lang.reflect.Type;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;

import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

public class WebApp{
	public native static int GetPort();
	public native static String GetId();
	public native static String GetHost();
	public native static String GetPath();
	public native static String GetSequence();
	public native static void Loop(String path);
	public native static void SetLogFlag(int flag);
	public native static void SetClassPath(String path);
	public native static String GetMimeType(String key);
	public native static String GetParameter(String key);
	public native static String GetRouteHost(String path);
	public native static void Trace(int level, String msg);
	public native static String GetConfig(String name, String key);
	public native static String GetWebAppPath(String path, String filename);

	public native static void SetCgiAccess(String path, String access);
	public native static void SetCgiExtdata(String path, String extdata);
	public native static void SetCgiDoc(String path, String reqdoc, String rspdoc, String remark);

	public native static int DisableSession(String sid);
	public native static String GetSession(String sid, String key);
	public native static int CreateSession(String sid, int timeout);
	public native static int SetSession(String sid, String key, String val);

	public native static int GetLastRemoteStatus();
	public native static String GetRemoteResult(String path, String param, String contype, String cookie);

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface Path{
		String value();
		String access() default "private";
	}

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface TimerTask{
		int value();
	}

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface DailyTask{
		String value();
	}

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface Document{
		Class request();
		Class response();
		String remark();
	}

	String sid = null;
	HttpRequest request = null;
	HttpResponse response = null;

	public byte[] process(byte[] msg){
		byte[] res = null;

		request = new HttpRequest(msg);
		response = new HttpResponse();

		try{
			process(request, response);
			res = response.getBytes();
		}
		catch(Utils.CommException e){
			JsonObject json = new JsonObject("{}");
			json.put("desc", e.getErrorString());
			json.put("code", e.getErrorCode());
			res = json.toString().getBytes();
			LogFile.Error(e);
		}
		catch(Exception e){
			JsonObject json = new JsonObject("{}");
			json.put("desc", "system error");
			json.put("code", Utils.SYSERR);
			res = json.toString().getBytes();
			LogFile.Error(e);
		}

		return res;
	}
	public String checkLogin() throws Exception{
		return checkLogin(null);
	}
	public String checkLogin(JsonObject data) throws Exception{
		sid = request.getSessionId();

		if (Utils.IsEmpty(sid)) Utils.Throw(Utils.TIMEOUT);

		String msg = GetRemoteResult("CheckLogin", "flag=C&sid=" + sid, null, request.getHeader("Cookie"));

		if (Utils.IsEmpty(msg)) Utils.Throw(Utils.SYSERR);

		if (data == null){
			data = new JsonObject(msg);
		}
		else{
			data.parse(msg);
		}

		if (!data.has("code")) Utils.Throw(Utils.SYSERR);
			
		int code = data.asInt("code");
		String desc = data.asString("desc");
		String user = data.asString("user");

		if (code < 0)  Utils.Throw(code, desc);

		if (Utils.IsEmpty(user)) Utils.Throw(Utils.TIMEOUT);

		return user;
	}
	public void process(HttpRequest request, HttpResponse response) throws Exception{
	}

	public static String GetClassPath(){
		String path = System.getProperty("java.class.path");

		if (Utils.IsNotEmpty(path)) return path;
		
		return GetResourceRootPath();
	}
	public static String GetResourceRootPath(){
		String path = ClassLoader.getSystemResource("").getPath();

		if (Utils.IsLinux()) return path;

		if (path.charAt(0) == '/' || path.charAt(0) == '\\'){
			path = path.substring(1);
		}

		return path;
	}
	public static String GetDocString(Class clazz) throws IllegalAccessException{
		class FieldItem{
			public String type = "";
			public String name = "";
			public String remark = "";
			public String extdata = "required";

			FieldItem(Field field){
				name = field.getName();
				type = field.getType().getSimpleName().toLowerCase();
				Required required = field.getAnnotation(Required.class);
				Optional optional = field.getAnnotation(Optional.class);

				if (required != null) remark = required.value();
				if (optional != null) remark = optional.value();
				if (required == null && optional != null) extdata = "optional";
			}
			public boolean isObject(){
				switch (type){
					case "int": return false;
					case "long": return false;
					case "short": return false;
					case "float": return false;
					case "double": return false;
					case "string": return false;
					case "integer": type = "int"; return false;
					case "boolean": type = "bool"; return false;
					default: return true;
				}
			}
		}

		String doc = "";
		Field[] vec = clazz.getFields();

		for (Field field : vec){
			FieldItem item = new FieldItem(field);

			if (item.isObject()){
				Type type = field.getGenericType();

				if (type instanceof ParameterizedType) {
					doc += String.format("\n<tr><td><span>%s</span></td><td>%s</td><td>%s</td><td>%s</td></tr>", item.name, "array", item.extdata, item.remark);
					doc += GetDocString((Class)((ParameterizedType)(type)).getActualTypeArguments()[0]).replace("\n<tr><td>", "\n<tr><td>&nbsp;&nbsp;<span>&gt;</span>");
				}
				else{
					doc += String.format("\n<tr><td><span>%s</span></td><td>%s</td><td>%s</td><td>%s</td></tr>", item.name, "object", item.extdata, item.remark);
					doc += GetDocString(field.getType()).replace("\n<tr><td>", "\n<tr><td>&nbsp;&nbsp;<span>&gt;</span>");
				}
			}
			else{
				doc += String.format("\n<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>", item.name, item.type, item.extdata, item.remark);
			}
		}

		while (true)
		{
			String tmp = doc.replace("<span>&gt;</span>&nbsp;&nbsp;", "<span>&nbsp;</span>&nbsp;&nbsp;");

			if (tmp.length() == doc.length()) return doc;

			doc = tmp;
		}
	}
	public static void Process(String path) throws IOException, FileNotFoundException{
		ConfigFile cfg = new ConfigFile();

		cfg.open(path = Utils.Translate(path));

		String apphome = cfg.get("PATH");
		String jvmpath = cfg.get("JAVA_CLASSPATH");

		System.load(Utils.Translate(apphome + "/etc/plugin/bin/InitSystem.so"));

		if (Utils.IsNotEmpty(jvmpath)) AddClassPath(jvmpath);

		WebApp.SetClassPath(GetClassPath());
		WebApp.SetLogFlag(2);
		WebApp.Loop(path);
	}
	public static void AddClassPath(String path){
		String classpath = System.getProperty("java.class.path");

		if (Utils.IsLinux()){
			classpath += ":" + path;
		}
		else{
			classpath += ";" + path;
		}

		System.setProperty("java.class.path", classpath);
	}

	public static String GetConfig(String key){
		return GetConfig(null, key);
	}
	public static String GetRemoteResult(String path){
		return GetRemoteResult(path, null, null, null);
	}
	public static String GetRemoteResult(String path, String param){
		return GetRemoteResult(path, param, null, null);
	}
	public static String GetRemoteResult(String path, String param, String contype){
		return GetRemoteResult(path, param, contype, null);
	}

	public static void main(String[] args) throws Exception{
		if (args.length < 1){
			System.out.println("please input configure filename");
			return;
		}

		Process(args[0]);
	}
}