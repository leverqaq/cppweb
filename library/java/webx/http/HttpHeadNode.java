package webx.http;

import java.util.Map;
import java.util.Set;
import java.util.HashMap;

public class HttpHeadNode{
	protected HashMap<String, String> map = new HashMap<String, String>();

	public int size(){
		return map.size();
	}
	public String toString(){
		String res = "";

		if (map.isEmpty()) return res;

		for (Map.Entry<String, String> item : map.entrySet()){
			res += "\r\n" + item.getKey() + ": " + item.getValue();
		}

		return res.substring(2);
	}
	public int parse(String msg){
		String[] vec = msg.split("\r\n");
		
		map.clear();

		for (String item : vec){
			String[] arr = item.split(":");
			if (arr.length < 2) continue;

			String key = arr[0].trim();
			if (key.isEmpty()) continue;

			String tmp = map.get(key);
			String val = arr[1].trim();

			if (tmp == null){
				map.put(key, val);
			}
			else{
				map.put(key, tmp + val);
			}
		}

		return map.size();
	}
	public Set<String> getKeys(){
		return map.keySet();
	}
	public String get(String key){
		return map.get(key);
	}
	public void set(String key, String val){
		set(key, val, false);
	}
	public void set(String key, String val, boolean append){
		if (append){
			String tmp = map.get(key);

			if (tmp == null){
				map.put(key, val);
			}
			else{
				map.put(key, tmp + val);
			}
		}
		else{
			map.put(key, val);
		}
	}
}