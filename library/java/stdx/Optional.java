package stdx;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Optional{
	String value();
	String regex() default "";
	long[] range() default {};
	long[] length() default {};
	String[] option() default {};
}