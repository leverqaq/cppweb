#ifndef XG_DB_POSTGRESQLCONNECTPOOL_CPP
#define XG_DB_POSTGRESQLCONNECTPOOL_CPP
//////////////////////////////////////////////////////////////
#include "../DBConnectPool.h"
#include "../PostgreSQLConnect.h"

class PostgreSQLConnectPool : public DBConnectPool
{
public:
	PostgreSQLConnectPool()
	{
		PostgreSQLConnect::Setup();
	}

protected:
	DBConnect* createConnect()
	{
		PostgreSQLConnect* conn = new PostgreSQLConnect();

		if (conn->init(cfg)) return conn;

		delete conn;

		return NULL;
	}
};

DEFINE_DBCONNECTPOOL_EXPORT_FUNC(PostgreSQLConnectPool)

//////////////////////////////////////////////////////////////
#endif