#ifndef XG_DB_CONNECTPOOL_H
#define XG_DB_CONNECTPOOL_H
////////////////////////////////////////////////////////
#include "DBConnect.h"
#include "../stdx/File.h"

class DBConnectPool : public ResPool<DBConnect>
{
protected:
	string charset;
	DBConnectConfig cfg;

	virtual DBConnect* createConnect() = 0;

public:
	DBConnectPool();
	sp<DBConnect> get();
	virtual bool init(const string& filepath);
	virtual bool init(const ConfigFile& file);
	
public:
	static DBConnectPool* Create(const ConfigFile& file);
};

typedef DBConnectPool* (*CREATE_DBCONNECTPOOL_FUNC)();
typedef void (*DESTROY_DBCONNECTPOOL_FUNC)(DBConnectPool*);


#define DEFINE_DBCONNECTPOOL_EXPORT_FUNC(__CLASS__)							\
EXTERN_DLL_FUNC DBConnectPool* CreateDBConnectPool()						\
{																			\
	return new __CLASS__();													\
}																			\
EXTERN_DLL_FUNC void DestroyDBConnectPool(DBConnectPool* pool)				\
{																			\
	delete pool;															\
}

////////////////////////////////////////////////////////
#endif
