import os;
import stdx;
import imgx;
import webx;
from PIL import ImageFont;

def GetColor(val):
	if isinstance(val, str):
		if len(val) <= 0: return None;
		elif val.isalnum(): val = '#' + val;
		else: val = tuple(map(lambda v: int(v), val.lstrip('([').rstrip('])').split(',')));
	return val;
			
def main(app):
	text = app.getParameter('text');
	size = app.getParameter('size');
	color = GetColor(app.getParameter('color'));
	background = GetColor(app.getParameter('background'));
	if color == None or len(color): color = '#55FF55';
	if text == None or len(text) == 0: text = 'XG';
	if isinstance(size, str) and size.isnumeric(): font = ImageFont.truetype(os.environ['SOURCE_HOME'] + "/source/font/logotext.ttf", int(size));
	else: font = None;
	img = imgx.TextLogo(text, None, font);
	app.setContentType(webx.GetMimeType(img.getFormat()));
	return img.draw(color, None, (-2.0, -1.5), background);