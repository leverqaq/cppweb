import dbx;
import webx;
import stdx;
import json;
import base64;

def main(app):
	res = '/res/menu/explorer.png';
	user = app.getSession('USER');
	dbid = app.getSession('DBID');
	if user == None: return res;
	else:
		id = app.getParameter('id');
		if not stdx.CheckLength(id, (1, 32)): res['code'] = stdx.XG_PARAMERR;
		else:
			hdr = user + '.WEBPAGE';
			try:
				with app.dbconnect(dbid) as db:
					data = db.queryArray('SELECT REMARK AS ICON FROM T_XG_PARAM WHERE ID=?', hdr + id);
					if len(data) > 0:
						img = data[0]['icon'];
						pos = img.find(':');
						if pos >= 0: 
							app.setHeader('Content-Type', img[0:pos]);
							return base64.b64decode(img[pos+1:]);
			except BaseException as e:
				app.trace('ERR', 'system error[%s]' % str(e));
	return res;