package example.app.javatest;

import stdx.Optional;
import stdx.Required;

public class Response{
	@Required("错误码")
	public int code;

	@Optional("错误描述")
	public String desc;

	@Optional("系统时间")
	public String datetime;
}