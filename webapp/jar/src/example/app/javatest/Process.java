package example.app.javatest;

import stdx.Utils;
import webx.WebApp;
import webx.LogFile;
import webx.json.JsonObject;
import webx.http.HttpRequest;
import webx.http.HttpResponse;

import example.dao.SystemDao;

@WebApp.Path(value = "${package}", access = "public")
@WebApp.Document(request = Request.class, response = Response.class, remark = "JAVA示例接口")
public class Process extends WebApp{
	public void process(Request request, Response response) throws Exception{
		LogFile.Trace("user[%s]name[%s]", request.user, request.name);

		response.code = Utils.OK;
		response.desc = "success";
		response.datetime = SystemDao.GetSystemTime();
	}
	public void process(HttpRequest request, HttpResponse response) throws Exception{
		Response resdata = new Response();
		process(request.toObject(Request.class), resdata);
		response.setBody(JsonObject.FromObject(resdata).toString());
	}
}