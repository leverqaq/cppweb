package example.dao;

import stdx.Utils;
import webx.utils.DBConnect;

public class SystemDao {
	public static String GetSystemTime() throws Exception{
		return DBConnect.Select(String.class, "SELECT ?", Utils.GetDateTimeString());
	}
}