#ifndef XG_WEBX_ROUTE_H
#define XG_WEBX_ROUTE_H
///////////////////////////////////////////////////////////
#include "std.h"

namespace webx
{
	int GetLastRemoteStatus();
	HostItem GetRegCenterHost();
	void CheckSystemRight(ProcessBase* proc);
	HostItem GetRouteHost(const string& path);
	int UpdateRouteList(const string& host, int port);
	int Broadcast(const string& path, const string& param = "", const string& contype = "", const string& cookie = "");
	SmartBuffer GetRemoteResult(const string& path, const string& param = "", const string& contype = "", const string& cookie = "");
	int NotifyHost(const string& host, int port, const string& path, const string& param = "", const string& contype = "", const string& cookie = "");
	void GetRemoteResult(const string& path, JsonReflect& response, const Object& param = JsonReflect(), const string& contype = "", const string& cookie = "");

	void RemoveConfileCache(const string& name);
	sp<ConfigFile> GetConfile(const string& name);
	string GetConfig(const string& name, const string& key = "");
	int SyncRemoteConfig(const string& type, const string& name);
	string GetRemoteConfig(const string& type, const string& name);
};

///////////////////////////////////////////////////////////
#endif
