#ifndef XG_WEBX_ROUTE_CPP
#define XG_WEBX_ROUTE_CPP
///////////////////////////////////////////////////////////
#include "../menu.h"
#include "../route.h"
#include "../../dbentity/T_XG_CONF.h"
#include "../../dbentity/T_XG_DBETC.h"

int webx::GetLastRemoteStatus()
{
	typedef int (*GetRemoteStatusFunc)();
	static GetRemoteStatusFunc func = (GetRemoteStatusFunc)Process::GetObject("HTTP_GET_LAST_REMOTE_STATUS_FUNC");

	return func ? func() : XG_FAIL;
}
HostItem webx::GetRegCenterHost()
{
	int port;
	char host[64];
	HostItem item;
	typedef int (*GetRegCenterHostFunc)(char*, int*);
	static GetRegCenterHostFunc func = (GetRegCenterHostFunc)Process::GetObject("HTTP_GET_REG_CENTER_HOST_FUNC");

	if (func == NULL) return item;

	if (func(host, &port) > 0)
	{
		item.host = host;
		item.port = port;
	}

	return item;
}

void webx::CheckSystemRight(ProcessBase* proc)
{
	try
	{
		proc->checkLogin();
		proc->checkSystemRight();
	}
	catch(Exception e)
	{
		HostItem route = GetRegCenterHost();
		const string& path = proc->getRequest()->getPath();
		const string& host = proc->getResponse()->getClientHost();

		if (host.empty()) return stdx::Throw(XG_AUTHFAIL, "permission denied");

		if (IsLocalHost(host.c_str()))
		{
			LogTrace(eIMP, "request[%s] from local host", path.c_str());
		}
		else if (route.equals(host))
		{
			LogTrace(eIMP, "request[%s] from route center", path.c_str());
		}
		else
		{
			sp<DBConnect> dbconn = webx::GetDBConnect();
			sp<QueryResult> rs = dbconn->query("SELECT ID FROM T_XG_ROUTE WHERE HOST=? AND ENABLED>0", host);

			if (rs && rs->next())
			{
				LogTrace(eIMP, "request[%s] from trust host[%s]", path.c_str(), host.c_str());
			}
			else
			{
				throw e;
			}
		}
	}
}

HostItem webx::GetRouteHost(const string& path)
{
	int port;
	char host[64];
	HostItem item;
	typedef int (*GetRouteHostFunc)(const char*, char*, int*);
	static GetRouteHostFunc func = (GetRouteHostFunc)Process::GetObject("HTTP_GET_ROUTE_HOST_FUNC");

	if (func == NULL) return item;

	if (func(path.c_str(), host, &port) > 0)
	{
		item.host = host;
		item.port = port;
	}

	return item;
}

int webx::UpdateRouteList(const string& host, int port)
{
	typedef int (*UpdateRouteListFunc)(const char*, int);
	static UpdateRouteListFunc func = (UpdateRouteListFunc)Process::GetObject("HTTP_UPDATE_ROUTE_LIST_FUNC");

	if (func == NULL) return XG_SYSERR;

	return func(host.c_str(), port);
}

SmartBuffer webx::GetRemoteResult(const string& path, const string& param, const string& contype, const string& cookie)
{
	typedef SmartBuffer (*GetRemoteResultFunc)(const char*, const char*, const char*, const char*);
	static GetRemoteResultFunc func = (GetRemoteResultFunc)Process::GetObject("HTTP_GET_REMOTE_RESULT_FUNC");

	if (func == NULL) return SmartBuffer();

	return func(path.c_str(), param.c_str(), contype.c_str(), cookie.c_str());
}

void webx::GetRemoteResult(const string& path, JsonReflect& response, const Object& param, const string& contype, const string& cookie)
{
	SmartBuffer data = GetRemoteResult(path, param.toString(), contype, cookie);

	if (data.str() && response.fromString(data.str())) return;

	int code = GetLastRemoteStatus();

	stdx::Throw(code < 0 ? code : XG_ERROR, "grasp remote request[" + path + "] failed");
}

int webx::Broadcast(const string& path, const string& param, const string& contype, const string& cookie)
{
	typedef int (*BroadcastFunc)(const char*, const char*, const char*, const char*);
	static BroadcastFunc func = (BroadcastFunc)Process::GetObject("HTTP_BROADCAST_HOST_FUNC");

	if (func == NULL) return XG_SYSERR;

	return func(path.c_str(), param.c_str(), contype.c_str(), cookie.c_str());
}

int webx::NotifyHost(const string& host, int port, const string& path, const string& param, const string& contype, const string& cookie)
{
	typedef int (*NotifyHostFunc)(const char*, int, const char*, const char*, const char*, const char*);
	static NotifyHostFunc func = (NotifyHostFunc)Process::GetObject("HTTP_NOTIFY_HOST_FUNC");

	if (func == NULL) return XG_SYSERR;

	return func(host.c_str(), port, path.c_str(), param.c_str(), contype.c_str(), cookie.c_str());
}

typedef TSMap<string, sp<ConfigFile>> ConfileMap;

static ConfileMap* GetConfileMap()
{
	XG_DEFINE_GLOBAL_VARIABLE(ConfileMap);
}

static string GetConfileKey(const string& name)
{
	return "SYSTEM_CONFILE[" + name + "]";
}

void webx::RemoveConfileCache(const string& name)
{
	if (name.empty())
	{
		GetConfileMap()->clear();
	}
	else
	{
		GetConfileMap()->remove(GetConfileKey(name));
	}
}

sp<ConfigFile> webx::GetConfile(const string& name)
{
	string content;
	sp<ConfigFile> cfg;

	if (GetConfileMap()->get(GetConfileKey(name), cfg)) return cfg;

	if (cfg) return cfg;

	auto dbconn = webx::GetDBConnect();
	string sqlcmd = "SELECT CONTENT FROM T_XG_CONF WHERE FOLDER='WEBPAGE' AND TITLE=?";

	if (dbconn->select(content, sqlcmd, name) < 0) stdx::Throw(XG_SYSERR, "load confile[" + name + "] failed");

	if (content.empty())
	{
		int res = 0;
		CT_XG_CONF tab;
		JsonElement json(GetRemoteConfig("confile", name));

		tab.init(dbconn);

		tab.user = "system";
		tab.icon = "/res/img/note/code.png";
		tab.title = json["title"].asString();
		tab.folder = json["folder"].asString();
		tab.content = content = json["content"].asString();

		tab.level = 1;
		tab.statetime.update();

		for (int i = 0; i < 5; i++)
		{
			tab.id = DateTime::GetBizId();
			tab.position = (int)(time(NULL));

			if ((res = tab.insert()) >= 0) break;
		}

		if (res < 0) stdx::Throw(XG_SYSERR, "sync confile[" + name + "] failed");

		LogTrace(eIMP, "sync confile[%s] success", name.c_str());
	}

	cfg = newsp<ConfigFile>();

	cfg->parse(content);

	GetConfileMap()->set(GetConfileKey(name), cfg);

	return cfg;
}

string webx::GetConfig(const string& name, const string& key)
{
	static ConfigFile* confile = HttpServer::Instance()->getConfigFile();

	if (name.empty()) return confile->getVariable(key);
	if (key.empty()) return confile->getVariable(name);

	return GetConfile(name)->getVariable(key);
}

int webx::SyncRemoteConfig(const string& type, const string& name)
{
	auto sync = [&](const string& type){
		int num = 0;
		sp<DBConnect> dbconn = webx::GetDBConnect();

		if (type == "confile")
		{
			auto synconfile = [&](const string& name){
				JsonElement json(GetRemoteConfig(type, name));

				string title = json["title"].asString();
				string folder = json["folder"].asString();
				string content = json["content"].asString();

				if (dbconn->execute("UPDATE T_XG_CONF SET CONTENT=? WHERE FOLDER=? AND TITLE=?", content, folder, title) < 0)
				{
					LogTrace(eERR, "sync configure[confile][%s] failed", title.c_str());
				}
				else
				{
					LogTrace(eIMP, "sync configure[confile][%s] success", title.c_str());

					RemoveConfileCache(title);

					++num;
				}
			};

			if (name.empty())
			{
				sp<QueryResult> rs = dbconn->query("SELECT TITLE FROM T_XG_CONF WHERE FOLDER='WEBPAGE'");

				if (rs)
				{
					sp<RowData> row = rs->next();

					while (row)
					{
						string name = row->getString(0);

						row = rs->next();

						synconfile(name);
					}
				}
			}
			else
			{
				synconfile(name);
			}
		}
		else
		{
			JsonElement json(GetRemoteConfig(type, name));

			json = json.get("list");

			CHECK_FALSE_RETURN(json.isArray());

			for (auto item : json)
			{
				string id = item["id"].asString();

				if (name.empty() || id == name)
				{
					CT_XG_DBETC tab;

					tab.init(dbconn);

					for (auto tmp : item)
					{
						string key = tmp.getName();

						tab.setValue(stdx::toupper(key), tmp.asString());
					}

					if (tab.update() < 0)
					{
						LogTrace(eERR, "sync configure[database][%s] failed", id.c_str());

						return false;
					}

					LogTrace(eIMP, "sync configure[database][%s] success", id.c_str());

					typedef DBConnectPool* (*GetDBConnectPoolFunc)(const char*);

					static GetDBConnectPoolFunc getPool = (GetDBConnectPoolFunc)Process::GetObject("HTTP_GET_DBCONNECT_POOL");

					if (tab.find() && tab.next() && tab.enabled.val() > 0)
					{
						DBConnectPool* pool = getPool(id.c_str());

						if (pool)
						{
							ConfigFile cfg;
							string key = "DLLPATH_" + tab.type.val();
							string path = webx::GetParameter(stdx::toupper(key));

							if (path.empty())
							{
								path = stdx::tolower(tab.type.val());
								path = stdx::translate("$PRODUCT_HOME/dll/libdbx." + path + "pool.so");
							}

							cfg.setVariable("DLLPATH", path);
							cfg.setVariable("HOST", tab.host.val());
							cfg.setVariable("USER", tab.user.val());
							cfg.setVariable("NAME", tab.name.val());
							cfg.setVariable("PORT", (int)tab.port.val());
							cfg.setVariable("CHARSET", tab.charset.val());
							cfg.setVariable("PASSWORD", tab.passwd.val());

							pool->init(cfg);
						}

						++num;
					}
				}
			}
		}

		return true;
	};

	int res = 0;

	if (type.empty())
	{
		if (sync("database")) res++;
		if (sync("confile")) res++;
	}
	else
	{
		if (sync(type)) res++;
	}

	if (res == 0) stdx::Throw(XG_ERROR, "sync configure failed");

	if (type == "database") webx::ReloadSystemConfig();

	return res;
}

string webx::GetRemoteConfig(const string& type, const string& name)
{
	HostItem route = webx::GetRegCenterHost();

	for (int i = 0; i < 5 && route.host.empty(); i++)
	{
		sleep(1);

		route = webx::GetRegCenterHost();
	}

	if (route.host.empty()) stdx::Throw(XG_SYSERR, "route center not found");

	HttpRequest request;

	if (type == "database")
	{
		request.init("getdatabaseList");
		request.setParameter("id", name);
	}
	else if (type == "confile")
	{
		request.init("confile/sharenote");
		request.setParameter("flag", "Q");
		request.setParameter("title", name);
	}
	else
	{
		stdx::Throw(XG_PARAMERR, "invalid parameter[type]");
	}

	SmartBuffer data = request.getResult(route.host, route.port);

	if (data.isNull()) stdx::Throw(XG_NETERR, "request[" + type + "] failed");

	return data.str();
}
///////////////////////////////////////////////////////////
#endif
