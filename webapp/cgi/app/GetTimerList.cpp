#include <webx/menu.h>
#include <webx/route.h>
#include <dbentity/T_XG_DBETC.h>

class GetTimerList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetTimerList)

int GetTimerList::process()
{
	param_int(status);
	param_string(path);

	webx::CheckFileName(path, 0);
	webx::CheckSystemRight(this);

	string sqlcmd;
	sp<DBConnect> dbconn = webx::GetDBConnect();

	if (status == 0)
	{
		stdx::format(sqlcmd, "SELECT * FROM T_XG_TIMER WHERE PATH LIKE '%%%s%%' ORDER BY PATH ASC", path.c_str());
	}
	else if (status == 1)
	{
		stdx::format(sqlcmd, "SELECT * FROM T_XG_TIMER WHERE STATUS<0 AND PATH LIKE '%%%s%%' ORDER BY PATH ASC", path.c_str());
	}
	else if (status == 2)
	{
		stdx::format(sqlcmd, "SELECT * FROM T_XG_TIMER WHERE STATUS=0 AND PATH LIKE '%%%s%%' ORDER BY PATH ASC", path.c_str());
	}
	else
	{
		stdx::format(sqlcmd, "SELECT * FROM T_XG_TIMER WHERE STATUS>0 AND PATH LIKE '%%%s%%' ORDER BY PATH ASC", path.c_str());
	}

	json["code"] = webx::PackJson(dbconn->query(sqlcmd), "list", json);
	json["datetime"] = DateTime::ToString();
	out << json;

	return XG_OK;
}