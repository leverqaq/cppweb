#include <webx/menu.h>
#include <webx/route.h>
#include <dbentity/T_XG_ROUTE.h>

class GetRouteList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetRouteList)

int GetRouteList::process()
{
	param_string(flag);
	param_string(name);
	param_string(host);

	webx::CheckSystemRight(this);

	int res = 0;
	string sqlcmd;
	sp<DBConnect> dbconn = webx::GetDBConnect();

	if (flag == "A")
	{
		if (webx::IsFileName(name, 0) && webx::IsFileName(host, 0))
		{
			stdx::format(sqlcmd, "SELECT ID,NAME,HOST,PORT,REMARK,ENABLED,PROCTIME,STATETIME FROM T_XG_ROUTE WHERE NAME LIKE '%s%%' AND HOST LIKE '%s%%' ORDER BY NAME ASC,HOST ASC", name.c_str(), host.c_str());
		}
	}
	else
	{
		webx::CheckFileName(name);

		stdx::format(sqlcmd, "SELECT HOST,PORT,ENABLED FROM T_XG_ROUTE WHERE NAME='%s' AND ENABLED>0 ORDER BY HOST ASC", name.c_str());
	}

	if (sqlcmd.length() > 0)
	{
		res = webx::PackJson(dbconn->query(sqlcmd), "list", json);
	}

	json["datetime"] = DateTime::ToString();
	json["code"] = res;
	out << json;

	return XG_OK;
}