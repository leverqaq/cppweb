#include <webx/menu.h>
#include <webx/route.h>


class GetParamList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetParamList)

int GetParamList::process()
{
	param_int(pagenum);
	param_int(pagesize);

	if (pagenum < 0) pagenum = 0;
	if (pagesize < 1) pagesize = 10;
	if (pagesize > 100) pagesize = 100;

	param_string(id);
	param_string(name);

	webx::CheckFileName(id, 0);
	webx::CheckFileName(name, 0);
	webx::CheckSystemRight(this);

	int res = 0;
	int num = 0;
	string cond;
	string sqlcmd;
	sp<DBConnect> dbconn = webx::GetDBConnect();

	cond = "FROM T_XG_PARAM WHERE ID NOT LIKE '%.%' AND ID LIKE '" + id + "%' AND NAME LIKE '" + name + "%'";
	
	if (dbconn->select(num, "SELECT COUNT(ID) " + cond) < 0) return simpleResponse(XG_SYSERR);

	sqlcmd = "SELECT ID,NAME,PARAM,FILTER,REMARK,STATETIME " + cond + " ORDER BY ID ASC" + webx::GetLimitString(dbconn.get(), pagesize, pagenum);

	res = webx::PackJson(dbconn->query(sqlcmd), "list", json);

	json["count"] = num;
	json["code"] = res;
	out << json;

	return XG_OK;
}