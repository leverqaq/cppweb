<%@ path=${filename}%>
<%@ include="dbentity/T_XG_USER.h"%>
<%
	int res = 0;
	string name;
	string lang;
	string userdata;

	try
	{
		checkLogin();
		
		CT_XG_USER tab;
		sp<DBConnect> dbconn;

		try
		{
			dbconn = webx::GetDBConnect();
		}
		catch(Exception e)
		{
			clearResponse();
			
			return XG_SYSBUSY;
		}

		tab.init(dbconn);
		tab.user = user;

		if (webx::PackJson(tab.find(), "list", json) > 0)
		{
			userdata = json["list"][(int)(0)].toString();
			name = session->get("NAME");
		}
	}
	catch(Exception e)
	{
	}

	if (session) lang = session->get("LANGUAGE");

	string client = stdx::tolower(request->getHeadValue("User-Agent"));

	if (client.find("android") == string::npos && client.find("iphone") == string::npos && client.find("ipad") == string::npos)
	{
		client = "PC";
	}
	else
	{
		client = "MOBILE";
	}
%>

<table id='TitleTable' height='100%'>
	<tr>
<%if (name.length() > 0){%>
		<td><label class='TitleLink' id='WebSiteButton'>网址收藏</label></td>
		<td><label class='SpliterLabel' id='WebSiteSpliter'>|</label></td>
		<td><label class='TitleLink' id='AccountButton'>用户中心</label></td>
		<td><label class='SpliterLabel' id='AccountSpliter'>|</label></td>
<%}%>
		<td><label class='TitleLink' id='AboutButton'>关于我们</label></td>
		<td><label class='SpliterLabel' id='AboutSpliter'>|</label></td>
		<td><label class='TitleLink' id='ShowLoginPageButton'></label></td>
	</tr>
</table>

<script>
var userdata = null;
var logintext = ['登录', '退出'];

getVue('TitleTable');
setLanguage('<%=lang%>');
setCurrentUser('<%=user%>');

<%if (userdata.length() > 0){%>
	userdata = eval('(<%=userdata%>)');
<%}%>

<%if (client == "PC"){%>
	getHttpCacheResult('/product/getwebsitelist', null, function(data){
		if (data.code > 0){
			$('#WebSiteButton').click(function(){
				showWebSitePage(userdata);
			});
		}
		else {
			$('#WebSiteSpliter').remove();
			$('#WebSiteButton').remove();
		}
	});

	$('#AccountButton').click(function(){
		showUserPage(userdata);
	});
	
	$('#AboutButton').click(function(){
		showAboutPage();
	});
<%}else{%>
	$('#WebSiteSpliter').remove();
	$('#WebSiteButton').remove();
	$('#AccountSpliter').remove();
	$('#AccountButton').remove();
	$('#AboutSpliter').remove();
	$('#AboutButton').remove();
<%}%>

function showAboutPage(){
	window.open('/sharenote?title=ABOUT');
}

function showUserPage(userdata){
	var msg = getHttpResult('/app/workspace/pub/userinfo.htm');

	showToastMessage(msg, true, null, null, true);

	$('#UserInfoMailText').val(userdata.mail);
	$('#UserInfoUserText').val(userdata.user);
	$('#UserInfoNameText').val(userdata.name);
	$('#UserInfoPhoneText').val(userdata.phone);
	$('#UserInfoAddressText').val(userdata.address);
}

function showWebSitePage(userdata){
	var reg = new RegExp('Web', 'g');
	var msg = getHttpResult('/app/product/pub/websitepad.htm');

	showToastMessage(msg.replace(reg, 'WebDialog'), null, null, '60%', true);
	
	$('#WebDialogSiteMenuDiv').hide().parent().css('padding', '1vh 1vw');
}

$('#ShowLoginPageButton').text(logintext[<%=(name.empty() ? 0 : 1)%>]).click(function(){
<%if (name.empty()){%>
	showLoginDialog(true);
<%}else{%>
	showConfirmMessage('&nbsp;&nbsp;是否要退出当前登录？&nbsp;&nbsp;', '退出登录', function(flag){
		if (flag){
			getHttpResult('/CheckLogin', {flag: 'Q'});
			clearCookie();
			updateTitle();
			updateMenu();
		}
	});
<%}%>
});
</script>