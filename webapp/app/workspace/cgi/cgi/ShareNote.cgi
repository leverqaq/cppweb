<%@ path=${filename}%>
<%
	param_string(id);
	param_int(comment);
	param_string(dbid);
	param_string(flag);
	param_string(title);
	param_string(folder);
	param_string(footer);
	param_string(padding);
	param_string(background);

	webx::CheckAlnumString(id, 0);
	webx::CheckFileName(title, 0);
	webx::CheckFileName(folder, 0);

	if (title == "FOOTER" && footer.empty()) footer = "none";

	if (id.empty() && title.empty())
	{
		clearResponse();
		
		return XG_PARAMERR;
	}

	string client = stdx::tolower(request->getHeadValue("User-Agent"));

	if (client.find("android") == string::npos && client.find("iphone") == string::npos && client.find("ipad") == string::npos)
	{
		if (padding.empty()) padding = "3vh 12vw";

		if (footer.empty()) footer = "/ShareNote?flag=S&title=FOOTER";
	}
	else
	{
		if (padding.empty()) padding = "1vh 4vw";
	}

	string icon;
	string type;
	string user;
	string level;
	string sqlcmd;
	string content;
	string quoteid;
	string statetime;
	bool marked = false;
	
	try
	{
		checkLogin();
	}
	catch(Exception e)
	{
	}

	if (id.empty())
	{
		if (folder.empty())
		{
			sqlcmd = "SELECT ID,TYPE,USER,ICON,LEVEL,TITLE,FOLDER,CONTENT,STATETIME,USER FROM T_XG_NOTE WHERE FOLDER='WEBPAGE' AND TITLE='" + title + "'";

			quoteid = "sharenote?title=" + stdx::EncodeURL(title);
		}
		else
		{
			param_name_string(user);

			if (user.empty() && (user = this->user).empty()) user = "system";

			if (dbid.empty()) dbid = this->dbid;
	
			sqlcmd = "SELECT ID,TYPE,USER,ICON,LEVEL,TITLE,FOLDER,CONTENT,STATETIME,USER FROM T_XG_NOTE WHERE USER='" + user + "' AND FOLDER='" + folder + "' AND TITLE='" + title + "'";
		}

		if (dbid == "none" || dbid == "null") dbid.clear();
	}
	else
	{
		if (dbid.empty()) dbid = this->dbid;

		sqlcmd = "SELECT ID,TYPE,USER,ICON,LEVEL,TITLE,FOLDER,CONTENT,STATETIME FROM T_XG_NOTE WHERE ID='" + id + "'";

		if (dbid == "none" || dbid == "null") dbid.clear();

		quoteid = "sharenote?dbid=" + dbid + "&id=" + id;
	}

	sp<DBConnect> dbconn;

	try
	{
		dbconn = webx::GetDBConnect(dbid);
	}
	catch(Exception e)
	{
		clearResponse();

		return XG_SYSBUSY;
	}
	
	sp<QueryResult> rs = dbconn->query(sqlcmd);

	if (!rs)
	{
		clearResponse();
		
		return XG_SYSERR;
	}

	sp<RowData> row = rs->next();
		
	if (!row)
	{
		clearResponse();
	
		return XG_OK;
	}

	id = row->getString(0);
	type = row->getString(1);
	user = row->getString(2);
	icon = row->getString(3);
	level = row->getString(4);
	title = row->getString(5);
	folder = row->getString(6);
	content = row->getString(7);
	statetime = row->getString(8);

	if (flag == "S")
	{
		clearResponse();

		if (user == this->user || level[0] > '2')
		{
			if (type[0] > '0')
			{%>
				<div id='ShareNoteContentDiv<%=id%>'></div>
				<script>
					$('#ShareNoteContentDiv<%=id%>').html(markdown('<%=webx::GetScriptString(content)%>'));
				</script>
			<%}
			else
			{
				out << content;
			}
		}

		return XG_OK;
	}

	if (user == this->user || level[0] > '2')
	{
		if (type[0] > '0')
		{
			content = "markdown('" + webx::GetScriptString(content) + "')";
			marked = true;
		}
		else
		{
			content = "'" + webx::GetScriptString(content) + "'";
		}
	}
	else
	{
		 content = "<div id='ErrorMessageDiv'>作者未公开该文档<br/><a href='/'>进入寻根主页</a></div>";
		 content = "'" + webx::GetScriptString(content) + "'";
	}

	if (quoteid.empty()) comment = 0;

	if (footer == "none") footer.clear();

	if (background.empty()) background = "none";
%>
<!DOCTYPE HTML>
<html>
<head>
<title><%=title%></title>
<meta name='referrer' content='always'/>
<link rel='shortcut icon' href='<%=icon%>'/>
<meta name='keywords' content='<%=title%>'/>
<meta name='description' content='<%=folder%> <%=title%>'/>
<meta http-equiv='x-ua-compatible' content='ie=edge,chrome=1'>
<meta http-equiv='content-type' content='text/html; charset=utf-8'>
<meta name='viewport' content='width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no'/>

<link rel='stylesheet' type='text/css' href='/app/workspace/css/base.css'/>
<link rel='stylesheet' type='text/css' href='/res/lib/highlight/css/tomorrow.css'/>

<script>
if (typeof(require) == 'function') delete window.module;
</script>

<script src='/res/lib/utils.js.gzip'></script>
<script src='/res/lib/laydate/laydate.js.gzip'></script>
<script src='/res/lib/highlight/highlight.js.gzip'></script>

<style>
body{
	background: <%=background%>;
	background-attachment: fixed;
}
#ErrorMessageDiv{
	color: #B24;
	font-size: 2rem;
	font-weight: bold;
	text-shadow: 1px 1px 1px #444;
}
#ErrorMessageDiv a{
	font-size: 1rem;
	line-height: 40px;
	text-decoration: none;
	text-shadow: 1px 1px 1px #444;
}
#ShareNoteDiv<%=id%>{
	padding: <%=padding%>;
}
#ShareNoteFooterDiv<%=id%>{
	padding-top: 36px;
	padding-bottom: 36px;
}
#ShareNoteCommentDiv<%=id%>{
	padding-top: 36px;
	padding-bottom: 36px;
}

<%if (title == "FOOTER"){%>
#ShareNoteContentDiv<%=id%>{
	padding: 0px;
}
<%}else{%>
#ShareNoteContentDiv<%=id%>{
	padding: 1vh 2vw;
	background: #FFF;
	border-radius: 8px;
}
<%}%>

<%if (marked){%>
#ShareNoteContentDiv<%=id%> th{
	font-size: 13px;
	min-width: 80px;
	text-align: left;
	padding: 4px 8px;
	font-weight: bold;
	border-bottom: 2px solid #555;
}
#ShareNoteContentDiv<%=id%> td{
	font-size: 12px;
	min-width: 80px;
	text-align: left;
	padding: 4px 8px;
	border-bottom: 1px solid #888;
}
#ShareNoteContentDiv<%=id%> table{
	min-width: 70%;
}
#ShareNoteContentDiv<%=id%> th:nth-child(even){
	min-width: 150px;
}
#ShareNoteContentDiv<%=id%> td:nth-child(even){
	min-width: 150px;
}
<%}%>

</style>

<script>
$(document).ready(function(){
<%if (marked){%>
	marked.setOptions({
		highlight: function(code){
			return hljs.highlightAuto(code).value;
		}
	});
<%}%>

	$('#ShareNoteContentDiv<%=id%>').html(<%=content%>);
	
<%if (footer.length() > 0){%>
	$('#ShareNoteFooterDiv<%=id%>').load('<%=footer%>');
<%}%>

<%if (comment > 0){%>
	setTimeout(function(){
		$('#ShareNoteCommentDiv<%=id%>').html(getHttpResult('/comment/commentlist', {quoteid: '<%=quoteid%>'}));
	}, 10);
<%}%>
});
</script>
</head>

<body>
	<div id='ShareNoteDiv<%=id%>'>
		<div id='ShareNoteContentDiv<%=id%>'></div>
<%if (comment > 0){%>
		<div id='ShareNoteCommentDiv<%=id%>'></div>
<%}%>
<%if (footer.length() > 0){%>
		<div id='ShareNoteFooterDiv<%=id%>'></div>
<%}%>
	</div>
</body>
</html>