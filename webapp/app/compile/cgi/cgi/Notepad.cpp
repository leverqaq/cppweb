#include <webx/menu.h>



class Notepad : public webx::ProcessBase
{
protected:
	int process();
};

int Notepad::process()
{
	param_string(id);
	param_string(name);
	param_string(level);
	param_string(title);
	param_string(folder);
	param_string(deficon);

	if (name.empty())
	{
		clearResponse();

		return XG_PARAMERR;
	}

	if (id.empty() && folder.empty())
	{
		clearResponse();

		return XG_PARAMERR;
	}
	
	try
	{
		checkLogin();
	}
	catch(Exception e)
	{
		out << "<script>sessionTimeout()</script>";

		return XG_NOTFOUND;
	}

	if (deficon.empty()) deficon = "/res/img/note/code.png";
	out<<"<style>\n.OutputPage{\n\tpadding: 0px 4px;\n\tword-wrap: break-word;\n\twhite-space: pre-wrap;\n}\n#OutputText{\n\theight: 20vh;\n\toverflow: scroll;\n\tmargin-top: 3px;\n\tbackground: #FFF;\n\tborder: 1px solid #CCC;\n\tbackground: rgba(255, 255, 255, 0.8);\n}\n#CodeContentDiv{\n\tmargin-top: 2px;\n\tborder: 1px solid #DDD;\n}\n.CodeMirror{\n\tbackground: rgba(255, 255, 255, 0.6);\n}\n.CodeMirror-gutters{\n\tbackground: rgba(255, 255, 255, 0.7);\n}\n</style>\n\n<table id=\'NoteEditTable\'>\n\t<tr>\n\t\t<td id=\'NoteListTd\'>\n\t\t\t<div id=\'NoteListDiv\'>\n\t\t\t\t<table id=\'NoteListTable\'></table>\n\t\t\t</div>\n\t\t</td>\n\t\t<td id=\'NoteEditTd\'>\n\t\t\t<table>\n\t\t\t\t<tr>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<v-text id=\'NoteTitleText\' title=\'";
	out<<(name);
	out<<"名称\' maxlength=\'20\'></v-text>\n\t\t\t\t\t\t<v-select id=\'NoteLevelSelect\' title=\'";
	out<<(name);
	out<<"级别\' option=\'系统|普通|推荐|公开\'></v-select>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td style=\'padding-left:16px\'>\n\t\t\t\t\t\t<span title=\'点击上传";
	out<<(name);
	out<<"图标\' id=\'NoteIcon\'></span>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<button class=\'TextButton\' style=\'margin-left:24px\' id=\'SaveNoteButton\'>保存</button>\n\t\t\t\t\t\t<button class=\'TextButton\' style=\'margin-left:4px;color:#008800\' id=\'AddNoteButton\'>新建</button>\n\t\t\t\t\t\t<button class=\'TextButton\' style=\'margin-left:4px;color:#DD2233\' id=\'DeleteNoteButton\'>删除</button>\n\t\t\t\t\t\t<button class=\'TextButton\' style=\'margin-left:24px\' id=\'CopyNoteLinkButton\' title=\'点击复制分享链接\'>分享链接</button>\n\t\t\t\t\t\t<button class=\'TextButton\' style=\'margin-left:4px;color:#008800\' id=\'CompileNoteButton\' title=\'点击编译源代码\'>编译执行</button>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t\t<div id=\'CodeContentDiv\'><textarea id=\'NoteContentText\'></textarea></div>\n\t\t\t<div id=\'OutputText\'></div>\n\t\t</td>\n\t</tr>\n</table>\n\n<script>\ngetVue(\'NoteEditTable\');";
	if (title.length() > 0 || id.length() > 0){	out<<"\t$(\'#DeleteNoteButton\').attr(\'disabled\', true);\n\t$(\'#NoteLevelSelect\').attr(\'disabled\', true);\n\t$(\'#NoteListTd\').hide();\n\t\n\t";
	if (id.empty()){	out<<"\t\t$(\'#NoteTitleText\').attr(\'disabled\', true);\n\t";
	}	}	out<<"var param = null;\nvar curid = null;\nvar notepad = null;\nvar curicon = null;\nvar curtitle = null;\nvar curlevel = null;\nvar titlelist = null;\nvar uploadicon = null;\nvar curcontent = null;\nvar curiconbtn = null;\nvar curnoteitem = null;\nvar curdatetime = null;\nvar selnoteitem = null;\nvar uploadimage = null;\n\ninitNotepad();\n\n$(\'#NoteLevelSelect option:first\').attr(\'disabled\', true);\n\nvar menubox = new ContextMenu(\'NoteListDiv\', [\'前移\', \'后移\'], function(text, elem){\n\telem = $.pack(elem);\n\tvar direct = text == \'前移\' ? \'U\' : \'D\';\n\n\tif (elem.attr(\'value\').indexOf(\'/\') >= 0){\n\t\tgetHttpResult(\'/compile/editnote\', {flag: \'M\', folder: elem.text(), direct: direct}, function(data){\n\t\t\tupdateSubMenu();\n\t\t});\n\t}\n\telse{\n\t\tgetHttpResult(\'/compile/editnote\', {flag: \'M\', id: elem.attr(\'value\'), direct: direct}, function(data){\n\t\t\tloadNoteItem();\n\t\t});\n\t}\n});";
	if (folder != "WEBPAGE"){	out<<"$(\".SubMenuItem\").each(function(){\n\tif ($(this).text() != \'新建目录\') menubox.bind(this);\n});";
	}	out<<"uploadimage = new UploadImageWidget(\'NoteIcon\', \'";
	out<<(name);
	out<<"图标\', \'14px\', 1024 * 1024, true);\n\nuploadimage.callback(function(data){\n\tif (data == null || data.code < 0){\n\t\tshowErrorToast(\'图片上传失败\');\n\t}\n\telse{\n\t\tuploadicon = data.url;\n\t}\n});\n\nuploadimage.image.parent().click(function(){\n\tvar iconpick = new IconPicker(uploadimage.image, \'note\');\n\t\n\tuploadimage.image.blur(function(){\n\t\tuploadicon = true;\n\t\ticonpick.hide();\n\t});\n});\n\n$(\'#CompileNoteButton\').click(function(){\n\tcompile();\n});\n\nfunction compile(){\n\tvar text = notepad.getValue();\n\tvar output = $(\'#OutputText\');\n\n\tif (strlen(text) == 0) return notepad.focus();\n\n\tshowToastMessage(\'正在编译代码...\');\n\n\toutput.html(\'\');\n\t\n\tgetHttpResult(\'/compile/compile\', {code: text}, function(data){\n\t\thideToastBox();\n\n\t\tmodifyNotepad(true);\n\n\t\tif (data.code == XG_PARAMERR){\n\t\t\tif (data.output){\n\t\t\t\toutput.html(\"<font color=\'#EE0000\'><pre class=\'OutputPage\'>\" + data.output + \"</pre></font>\");\n\t\t\t}\n\t\t\telse{\n\t\t\t\toutput.html(\"<font color=\'#EE0000\'><pre class=\'OutputPage\'>源码编译失败</pre></font>\");\n\t\t\t}\n\t\t}\n\t\telse if (data.code == XG_TIMEOUT){\n\t\t\toutput.html(\"<font color=\'#EE0000\'><pre class=\'OutputPage\'>执行超时(源码中可能存在耗时操作)</pre></font>\");\n\t\t}\n\t\telse if (data.code == XG_SYSBUSY){\n\t\t\tif (idx++ < 50){\n\t\t\t\tcompile();\n\t\t\t}\n\t\t\telse{\n\t\t\t\tshowToast(\'系统繁忙---请稍后再试\');\n\n\t\t\t\tidx = 0;\n\t\t\t}\n\t\t}\n\t\telse if (data.code < 0){\n\t\t\tif (data.status && data.status == 403){\n\t\t\t\tshowToast(\'访问频繁请稍后再试\');\n\t\t\t}\n\t\t\telse{\n\t\t\t\tshowToast(\'源码编译失败\');\n\t\t\t}\n\t\t}\n\t\telse{\n\t\t\toutput.html(\"<pre class=\'OutputPage\'>\" + data.output + \"</pre>\");\n\t\t}\n\t}, true);\n}\n\nfunction modifyNotepad(flag){\n\tvar width = getClientWidth();\n\tvar height = getClientHeight();\n\n\tvar w = 0;\n\tvar h = 0;\n\tvar cx = width - 220;\n\tvar cy = height - 140;\n\n\tif (getSideWidth) cx -= getSideWidth();\n\t\n\t$(\'#NoteListDiv\').height(cy + 28);\n\n\tif (flag){\n\t\tw = cx;\n\t\th = 160;\n\t\tcy = cy -  h;\n\t\t\n\t\t$(\'#OutputText\').width(w).height(h).show();\n\t}\n\telse{\n\t\t$(\'#OutputText\').hide();\n\t}\n\n\t$(\'#CodeContentDiv\').width(cx).height(cy);\n\n\tnotepad.setSize(\'100%\',\'100%\');\n}\nfunction initNotepad(){\n\tnotepad = CodeMirror.fromTextArea(document.getElementById(\'NoteContentText\'), {\n\t\tmode: \'text/x-csrc\',\n\t\tonBlur: notepadBlur,\n\t\ttabSize: 4,\n\t\tindentUnit: 4,\n\t\tinputStyle: \'textarea\',\n\t\tfoldGutter: true,\n\t\tlineNumbers: true,\n\t\tlineWrapping: true,\n\t\tmatchBrackets: true,\n\t\tindentWithTabs: true,\n\t\tstyleActiveLine: true,\n\t\tshowCursorWhenSelecting: true,\n\t\tgutters: [\'CodeMirror-linenumbers\', \'CodeMirror-foldgutter\']\n\t});\n\t\n\tmodifyNotepad(false);\n\n\tnotepad.on(\'blur\', notepadBlur);\n\n\treturn notepad;\n}\n\nfunction addNoteResult(flag){\n\tif (flag == 1) return editNoteItem(param, \'A\');\n}\n\nfunction delNoteResult(flag){\n\tif (flag == 1) return editNoteItem(param, \'D\');\n}\n\nfunction delNoteFolderResult(flag){\n\tif (flag == 1) return editNoteItem(param, \'R\');\n}\n\nfunction getContent(msg){\n\treturn notepad ? notepad.getValue() : null;\n}\n\nfunction setContent(msg){\n\tnotepad.setValue(msg);\n\tmodifyNotepad(false);\n\tsetNotepadFocus();\n}\n\nfunction setNotepadFocus(){\n\tnotepadBlur(notepad);\n}\n\nfunction notepadBlur(notepad){\n\tvar content = getContent();\n\tif (content != (curcontent || \'\')){\n\t\tsetSaveNeeded(function(func){\n\t\t\tvar icon = getBackgroundImage(uploadimage.image[0]);\n\t\t\tvar level = $(\'#NoteLevelSelect\').val();\n\t\t\tvar title = $(\'#NoteTitleText\').val();\n\n\t\t\tsetSaveNeeded(null);\n\n\t\t\tparam = {};\n\t\t\tparam[\'id\'] = curid;\n\t\t\tparam[\'level\'] = level;\n\t\t\tparam[\'title\'] = title;\n\t\t\tparam[\'content\'] = content;\n\t\t\tparam[\'folder\'] = \'";
	out<<(folder);
	out<<"\';\n\t\t\t\n\t\t\tif ((len = strlen(title)) == 0){\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"名称不能为空\');\n\t\t\t\t$(\'#NoteTitleText\').focus();\n\t\t\t}\n\t\t\telse if (len > 24){\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"名称最多12个汉字或24个英文字母\');\n\t\t\t\t$(\'#NoteTitleText\').focus();\n\t\t\t}\n\t\t\telse if ((len = strlen(content)) == 0){\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"内容不能为空\');\n\t\t\t\tsetNotepadFocus();\n\t\t\t}\n\t\t\telse if (len > 1024 * 1024){\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"内容太长\');\n\t\t\t\tsetNotepadFocus();\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\tparam[\'icon\'] = getString(icon);\n\n\t\t\t\tif (title == curtitle){\n\t\t\t\t\tshowConfirmMessage(\'";
	out<<(name);
	out<<"内容已修改，是否马上保存？\', \'保存选项\', function(flag){\n\t\t\t\t\t\tif (flag == 1) editNoteItem(param, \'U\');\n\t\t\t\t\t\tif (func) func();\n\t\t\t\t\t});\n\t\t\t\t}\n\t\t\t\telse{";
	if (title.length() > 0 || id.length() > 0){	out<<"\t\t\t\t\tshowConfirmMessage(\'";
	out<<(name);
	out<<"内容已修改，是否马上保存？\', \'保存选项\', function(flag){\n\t\t\t\t\t\tif (flag == 1) editNoteItem(param, \'U\');\n\t\t\t\t\t\tif (func) func();\n\t\t\t\t\t});";
	}else{	out<<"\t\t\t\t\tshowConfirmMessage(\'";
	out<<(name);
	out<<"名称已修改，是否新建";
	out<<(name);
	out<<"？\', \'是否新建\', function(flag){\n\t\t\t\t\t\tif (flag == 1) editNoteItem(param, \'A\');\n\t\t\t\t\t\tif (func) func();\n\t\t\t\t\t});";
	}	out<<"\t\t\t\t}\n\n\t\t\t\treturn true;\n\t\t\t}\n\t\t\t\n\t\t\tif (func) func();\n\t\t\t\n\t\t\treturn false;\n\t\t});\n\t}\n}\n\nfunction loadNoteItem(flag){\n\t$(\'#NoteEditTable\').hide();\n\n\tparam = {};\n\tparam[\'id\'] = \'";
	out<<(id);
	out<<"\';\n\tparam[\'level\'] = \'";
	out<<(level);
	out<<"\';\n\tparam[\'title\'] = \'";
	out<<(title);
	out<<"\';\n\tparam[\'folder\'] = \'";
	out<<(folder);
	out<<"\';\n\n\t$(\'#CopyNoteLinkButton\').attr(\'disabled\', true);\n\t$(\'#NoteLevelSelect\').val(1);\n\ttitlelist = \'|\';\n\n\tgetHttpResult(\'/compile/getnotelist\', param, function(data){\n\t\t$(\'#NoteListTable\').html(\'\');\n\n\t\tif (data.code == XG_TIMEOUT){\n\t\t\tsessionTimeout();\n\t\t}\n\t\telse if (data.code < 0){\n\t\t\tshowToast(\'加载数据失败\');\n\t\t}\n\t\telse if (data.code == 0){";
	if (title.length() > 0 || id.length() > 0){	out<<"\t\t\tshowToast(\'没有找到";
	out<<(name);
	out<<"数据\');";
	}else if (folder == "WEBPAGE"){	out<<"\t\t\t$(\'#NoteLevelSelect\').val(3);\n\t\t\t$(\'#DeleteNoteButton\').attr(\'disabled\', true);\n\t\t\tif (flag == \'D\'){\n\t\t\t\tcurid = null;\n\t\t\t}";
	}else{	out<<"\t\t\tif (flag == \'D\'){\n\t\t\t\tshowConfirmMessage(\'目录[\' + \'";
	out<<(folder);
	out<<"\' + \']\' + \'无";
	out<<(name);
	out<<"信息。<br>是否决定删除该目录？\', \'删除目录\', delNoteFolderResult);\n\t\t\t\tcurid = null;\n\t\t\t\tflag = false;\n\t\t\t}";
	}	out<<"\t\t\tuploadimage.image.css(\'background-image\', \'url(";
	out<<(deficon);
	out<<")\');\n\t\t\t$(\'#NoteTitleText\').val(\'\');\n\t\t\t$(\'#NoteEditTable\').show();\n\t\t\tsetContent(\'\');\n\t\t}\n\t\telse{\n\t\t\tcurdatetime = null;\n\t\t\tselnoteitem = null;\n\n\t\t\t$.each(data.list, function(idx, item){\n\t\t\t\ttitlelist += item.title + \'|\';\n\t\t\t\taddNoteItem(item.id, item.title, item.icon, item.level, item.statetime);\n\t\t\t});\n\t\t\t\n\t\t\tselectNoteItem(selnoteitem);\n\t\t\t\n\t\t\t$(\".NotepadItem\").each(function(){\n\t\t\t\tmenubox.bind(this);\n\t\t\t});\n\t\t}\n\t});\n\n\treturn flag;\n}\nfunction updateNoteInfo(note, level, title){\n\tlevel = getString(level);\n\t\n\tif (level == \'0\'){\n\t\tnote.children().last().html(title + \'<span>系统</span>\').children(\'span\').css(\'color\', \'#EE2233\');\n\t}\n\telse if (level ==\'1\'){\n\t\tnote.children().last().html(title + \'<span>普通</span>\').children(\'span\').css(\'color\', \'#000000\');\n\t}\n\telse if (level ==\'2\'){\n\t\tnote.children().last().html(title + \'<span>推荐</span>\').children(\'span\').css(\'color\', \'#000000\').css(\'font-weight\', \'bold\');\n\t}\n\telse{\n\t\tnote.children().last().html(title + \'<span>公开</span>\').children(\'span\').css(\'color\', \'#22BB22\');\n\t}\n}\nfunction addNoteItem(id, title, icon, level, statetime){\n\t$(\'#NoteListTable\').append(\"<tr class=\'NotepadItem\' id=\'Note\" + id + \"\' value=\'\" + id + \"\'><td class=\'NoteIconButton\' style=\'background-image:url(\" + icon + \")\'></td><td></td></tr>\");\n\t\n\tvar note = $(\'.NotepadItem\').last();\n\t\n\tif (curdatetime == null || curdatetime < statetime || selnoteitem == null){\n\t\tcurdatetime = statetime;\n\t\tselnoteitem = note;\n\t}\n\t\n\tupdateNoteInfo(note, level, title);\n\n\treturn note.click(function(){\n\t\tselectNoteItem($(this));\n\t});\n}\nfunction editNoteItem(param, flag){\n\tif (flag == \'A\') param[\'id\'] = \'\';\n\n\tif (flag == \'C\' || flag == \'D\' || flag == \'R\'){\n\t\tparam[\'title\'] = \'\';\n\t\tparam[\'content\'] = \'\';\n\t}\n\n\tparam[\'flag\'] = flag;\n\tparam[\'deficon\'] = \'";
	out<<(deficon);
	out<<"\';\n\t\n\tif (flag == \'U\' && uploadicon == null) param[\'icon\'] = \'\';\n\n\tgetHttpResult(\'/compile/editnote\', param, function(data){\n\t\tif (data.code == XG_TIMEOUT){\n\t\t\tsessionTimeout();\n\t\t}\n\t\telse if (data.code < 0){\n\t\t\tif (flag == \'A\'){\n\t\t\t\tshowToast(\'新建";
	out<<(name);
	out<<"失败\');\n\t\t\t}\n\t\t\telse if (flag == \'D\'){\n\t\t\t\tshowToast(\'删除";
	out<<(name);
	out<<"失败\');\n\t\t\t}\n\t\t\telse if (flag == \'U\'){\n\t\t\t\tshowToast(\'修改";
	out<<(name);
	out<<"失败\');\n\t\t\t}\n\t\t\telse if (flag == \'R\'){\n\t\t\t\tshowToast(\'删除目录失败\');\n\t\t\t}\n\t\t\telse{\n\t\t\t\tshowToast(\'保存";
	out<<(name);
	out<<"失败\');\n\t\t\t}\n\t\t}\n\t\telse{\n\t\t\tsetSaveNeeded(null);\n\t\t\t\n\t\t\tif (flag == \'R\'){\n\t\t\t\tselectMenu(curmenuitem, true);\n\t\t\t}\n\t\t\telse if (flag == \'A\'){\n\t\t\t\tshowToast(\'新建";
	out<<(name);
	out<<"成功\');\n\t\t\t\ttitlelist += param[\'title\'] + \'|\';\n\t\t\t\tselectNoteItem(addNoteItem(data.id, param[\'title\'], data.icon, param[\'level\'], data.statetime));\n\t\t\t\tmenubox.bind(\'Note\' + data.id);\n\t\t\t}\n\t\t\telse if (flag == \'U\'){\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"保存成功\');\n\t\t\t\t\n\t\t\t\tif (curtitle != param[\'title\']){\n\t\t\t\t\tvar pos = titlelist.indexOf(\'|\' + curtitle + \'|\');\n\t\t\t\t\tif (pos >= 0){\n\t\t\t\t\t\ttitlelist = titlelist.substring(0, pos) + param[\'title\'] + titlelist.substring(pos + curtitle.length);\n\t\t\t\t\t}\n\t\t\t\t}\n\t\t\t\t\n\t\t\t\tcurcontent = param[\'content\'] || curcontent;\n\t\t\t\tcurtitle = param[\'title\'];\n\t\t\t\tcurlevel = param[\'level\'];\n\t\t\t\tcuricon = param[\'icon\'];\n\n\t\t\t\tif (strlen(data.icon) > 0){\n\t\t\t\t\tuploadimage.image.css(\'background-image\', \'url(\' + data.icon + \')\');\n\t\t\t\t\tcuricon = data.icon;\n\t\t\t\t}\n\n\t\t\t\tif (strlen(curicon) > 0){\n\t\t\t\t\tcuriconbtn.css(\'background-image\', \'url(\' + curicon + \')\');\n\t\t\t\t}\n\t\t\t\t\n\t\t\t\tupdateNoteInfo(curnoteitem, curlevel, curtitle);\n\t\t\t}\n\t\t\telse{\n\t\t\t\tflag = loadNoteItem(flag);\n\t\t\t}\n\t\t}\n\t});\n\t\n\treturn flag;\n}\nfunction selectNoteItem(note){\n\tif (saveneeded){\n\t\tsaveneeded(function(){\n\t\t\tselectNoteItem(note);\n\t\t});\n\t\t\n\t\tsaveneeded = null;\n\t\treturn true;\n\t}\n\n\t$(\'.NotepadItem\').css(\'background\', \'none\');\n\tnote.css(\'backgroundColor\', \'rgba(0, 0, 0, 0.3)\');\n\tcuriconbtn = note.find(\'.NoteIconButton\');\n\tid = note.attr(\'value\');\n\tcurnoteitem = note;\n\tuploadicon = null;\n\tcurcontent = null;\n\tcurtitle = null;\n\tcurlevel = null;\n\tcurid = null;\n\n\tif (strlen(id) == 0){\n\t\tselectMenu(curmenuitem, true);\n\t}\n\telse{\n\t\tshowToastMessage(\'正在加载数据...\');\n\t\t$(\'#NoteTitleText\').val(\'\');\n\t\tparam = {};\n\t\tparam[\'id\'] = id;\n\t\tgetHttpResult(\'/compile/getnotecontent\', param, function(data){\n\t\t\thideToastBox();\n\t\t\tif (data.code == XG_TIMEOUT){\n\t\t\t\tsessionTimeout();\n\t\t\t}\n\t\t\telse if (data.code < 0){\n\t\t\t\tshowToast(\'加载数据失败\');\n\t\t\t}\n\t\t\telse{\n\t\t\t\tcurcontent = data.content;\n\t\t\t\tcurtitle = data.title;\n\t\t\t\tcurlevel = data.level;\n\t\t\t\tcuricon = data.icon;\n\n\t\t\t\tuploadimage.image.css(\'background-image\', \'url(\' + data.icon + \')\');\n\t\t\t\t$(\'#CopyNoteLinkButton\').removeAttr(\'disabled\');\n\t\t\t\t$(\'#NoteLevelSelect\').val(curlevel);\n\t\t\t\t$(\'#NoteTitleText\').val(curtitle);\n\t\t\t\t$(\'#NoteEditTable\').show();\n\n\t\t\t\tsetContent(curcontent);\n\n\t\t\t\tif (curid == null && navigator.userAgent.toLowerCase().indexOf(\"firefox\") >= 0){\n\t\t\t\t\tnotepad.fullscreen(true);\n\t\t\t\t\tnotepad.fullscreen(false);\n\t\t\t\t}\n\t\t\t\t\n\t\t\t\tcurid = data.id;\n\t\t\t\t";
	if (title.empty() && id.empty()){	out<<"\t\t\t\tif (curlevel == 0){\n\t\t\t\t\t$(\'#DeleteNoteButton\').attr(\'disabled\', true);\n\t\t\t\t\t$(\'#NoteLevelSelect\').attr(\'disabled\', true);\n\t\t\t\t\t$(\'#NoteTitleText\').attr(\'disabled\', true);\n\t\t\t\t}\n\t\t\t\telse{\n\t\t\t\t\t$(\'#DeleteNoteButton\').removeAttr(\'disabled\');\n\t\t\t\t\t$(\'#NoteLevelSelect\').removeAttr(\'disabled\');\n\t\t\t\t\t$(\'#NoteTitleText\').removeAttr(\'disabled\');\n\t\t\t\t\t$(\'#NoteIcon\').removeAttr(\'disabled\');\n\t\t\t\t}";
	}	out<<"\t\t\t}\n\t\t}, true);\n\t}\n}\n\n$(\'#DeleteNoteButton\').click(function(){\n\tif (curid == null){\n\t\tshowConfirmMessage(\'目录[\' + \'";
	out<<(folder);
	out<<"\' + \']\' + \'无";
	out<<(name);
	out<<"信息。<br>是否决定删除该目录？\', \'删除目录\', delNoteFolderResult);\n\t}\n\telse{\n\t\tparam = {};\n\t\tparam[\'id\'] = curid;\n\t\tshowConfirmMessage(\'是否决定删除当前";
	out<<(name);
	out<<"？\', \'删除";
	out<<(name);
	out<<"\', delNoteResult);\n\t}\n});\n\n$(\'#AddNoteButton\').click(function(){\n\tvar uploadbutton = null;\n\n\tshowConfirmMessage(\"<table id=\'AddNoteTable\' class=\'DialogTable\'><tr><td><v-select id=\'AddNoteLevelSelect\' title=\'";
	out<<(name);
	out<<"级别\' option=\'系统|普通|推荐|公开\'></v-select></td></tr><tr><td><v-text id=\'AddNoteTitleText\' title=\'";
	out<<(name);
	out<<"名称\' size=\'14\' maxlength=\'20\'></v-text></td></tr></table>\", \'添加";
	out<<(name);
	out<<"\', function(flag){\n\t\tif (flag == 0) return true;\n\n\t\tvar title = $(\'#AddNoteTitleText\').val();\n\t\tvar level = $(\'#AddNoteLevelSelect\').val();\n\n\t\tif ((len = strlen(title)) == 0){\n\t\t\t$(\'#AddNoteTitleText\').focus();\n\t\t\treturn false;\n\t\t}\n\t\telse if (len > 24){\n\t\t\tsetMessageErrorText(\'";
	out<<(name);
	out<<"名称太长\', $(\'#AddNoteTitleText\'));\n\t\t\treturn false;\n\t\t}\n\t\telse if (!isFileName(title)){\n\t\t\tsetMessageErrorText(\'名称不能有特殊字符\', $(\'#AddNoteTitleText\'));\n\t\t\treturn false;\n\t\t}\n\t\telse if (titlelist.indexOf(\'|\' + title + \'|\') >= 0){\n\t\t\tsetMessageErrorText(\'名称与现有";
	out<<(name);
	out<<"冲突\', $(\'#AddNoteTitleText\'));\n\t\t\treturn false;\n\t\t}\n\t\t\n\t\tparam = {};\n\t\tparam[\'level\'] = level;\n\t\tparam[\'title\'] = title;\n\t\tparam[\'folder\'] = \'";
	out<<(folder);
	out<<"\';\n\n\t\treturn editNoteItem(param, \'A\');\n\t});";
	if (folder == "WEBPAGE"){	out<<"\t$(\'#AddNoteLevelSelect\').val(3);";
	}else{	out<<"\t$(\'#AddNoteLevelSelect\').val(1).children(\'option\').first().attr(\'disabled\', true);";
	}	out<<"\t$(\'#AddMenuTitleText\').focus();\n});\n\n$(\'#SaveNoteButton\').click(function(){\n\tvar icon = getBackgroundImage(uploadimage.image[0]);\n\tvar level = $(\'#NoteLevelSelect\').val();\n\tvar title = $(\'#NoteTitleText\').val();\n\tvar content = getContent();\n\n\tsetSaveNeeded(null);\n\n\tparam = {};\n\tparam[\'id\'] = curid;\n\tparam[\'level\'] = level;\n\tparam[\'title\'] = title;\n\tparam[\'content\'] = content;\n\tparam[\'folder\'] = \'";
	out<<(folder);
	out<<"\';\n\t\n\tif ((len = strlen(title)) == 0){\n\t\tshowToast(\'";
	out<<(name);
	out<<"名称不能为空\');\n\t\t$(\'#NoteTitleText\').focus();\n\t}\n\telse if (len > 24){\n\t\tshowToast(\'";
	out<<(name);
	out<<"名称最多12个汉字或24个英文字母\');\n\t\t$(\'#NoteTitleText\').focus();\n\t}\n\telse if ((len = strlen(content)) == 0){\n\t\tshowToast(\'";
	out<<(name);
	out<<"内容不能为空\');\n\t\tsetNotepadFocus();\n\t}\n\telse if (len > 1024 * 1024){\n\t\tshowToast(\'";
	out<<(name);
	out<<"内容太长，请分段保存\');\n\t\tsetNotepadFocus();\n\t}\n\telse{\n\t\tif (uploadicon == null) icon = curicon;\n\n\t\tparam[\'icon\'] = getString(icon);\n\n\t\tif (strlen(curid) == 0){\n\t\t\teditNoteItem(param, \'A\');\n\t\t}\n\t\telse if (title == curtitle){\n\t\t\tif (icon == curicon && level == curlevel && content == curcontent){\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"内容未更新\');\n\t\t\t}\n\t\t\telse{\n\t\t\t\tif (content == curcontent) delete param[\'content\'];\n\t\t\t\teditNoteItem(param, \'U\');\n\t\t\t}\n\t\t}\n\t\telse{\n\t\t\tif (titlelist.indexOf(\'|\' + title + \'|\') >= 0){\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"名称与现有";
	out<<(name);
	out<<"冲突\');\n\t\t\t\t$(\'#NoteTitleText\').focus();\n\t\t\t}\n\t\t\telse{\n\t\t\t\teditNoteItem(param, \'U\');\n\t\t\t}\n\t\t}\n\t}\n});\n\n$(\'#CopyNoteLinkButton\').click(function(){\n\tvar clipboard = new Clipboard(\'#CopyNoteLinkButton\', {\n\t\ttext: function(){\n\t\t\tvar host = window.location.href;\n\t\t\tvar link = \'/compile/mainframe?path=/compile/sharenote\' + encodeURIComponent(\'?flag=S&dbid=";
	out<<(dbid);
	out<<"&id=\' + curid) + \'&title=\' + encodeURIComponent(curtitle);\n\t\t\tif (strlen(curicon) < 128) link += \'&icon=\' + curicon;\n\t\t\tshowToast(\"<a class=\'TextLink\' href=\'\" + link + \"\' target=\'_blank\'>";
	out<<(name);
	out<<"分享链接已经复制到剪切板</a>\", 3000);\n\t\t\treturn host.indexOf(\'/\', 8) > 0 ? host.substr(0, host.indexOf(\'/\', 8)) + link : host + link;\n\t\t}\n\t});\n});\n\nloadNoteItem();\n</script>";


	return XG_OK;
}
HTTP_WEBAPP(Notepad, CGI_PRIVATE, "/compile/${filename}")