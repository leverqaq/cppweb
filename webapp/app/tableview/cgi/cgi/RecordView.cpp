#include <webx/menu.h>



class RecordView : public webx::ProcessBase
{
protected:
	int process();
};

int RecordView::process()
{
	out<<"<div></div>";
	param_string(flag);
	param_string(tabid);

	auto add = [&](){
	};

	auto remove = [&](){
	};

	auto update = [&](){
	};

	if (flag.length() > 0)
	{
		checkLogin();
		checkSystemRight();

		if (flag == "A")
		{
			add();
		}
		else if (flag == "D")
		{
			remove();
		}
		else if (flag == "U")
		{
			update();
		}

		return simpleResponse(XG_OK);
	}

	SmartBuffer content;

	webx::CheckFileName(tabid);

	if (app->getFileContent(app->getPath() + "app/tableview/pub/recordview.htm", content) <= 0) return clearResponse();

	string msg = stdx::replace(content.str(), "${recordoperhtml}", webx::GetScriptString(out.toString()));

	out.setContent(stdx::replace(msg, "${tabid}", tabid));
	out<<"<script>\n$recordvmdata.button = [];\n</script>";


	return XG_OK;
}
HTTP_WEBAPP(RecordView, CGI_PRIVATE, "/tableview/${filename}")