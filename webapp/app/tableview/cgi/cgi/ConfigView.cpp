#include <webx/menu.h>



class ConfigView : public webx::ProcessBase
{
protected:
	int process();
};

int ConfigView::process()
{
	out<<"<div><v-button id=\'AddRecordButton\' title=\'添加\'></v-button></div>";
	param_string(id);
	param_string(flag);
	param_string(name);
	param_string(dbid);
	param_string(title);
	param_string(remark);

	auto add = [&](){
		return webx::GetDBConnect()->execute("INSERT INTO T_XG_TABETC(ID,DBID,NAME,TITLE,REMARK,ENABLED,STATETIME) VALUES(?,?,?,?,?,2,?)", id, dbid, name, title, remark, DateTime::ToString());
	};

	auto remove = [&](){
		int res = webx::GetDBConnect()->execute("DELETE FROM T_XG_TABETC WHERE ID=? AND ENABLED>1", id);

		if (res > 0) webx::GetDBConnect()->execute("DELETE FROM T_XG_TABCOLS WHERE TABID=?", id);

		return res;
	};

	auto update = [&](){
		return webx::GetDBConnect()->execute("UPDATE T_XG_TABETC SET DBID=?,NAME=?,TITLE=?,REMARK=?,STATETIME=? WHERE ID=? AND ENABLED>1", dbid, name, title, remark, DateTime::ToString(), id);
	};

	if (flag.length() > 0)
	{
		checkLogin();
		checkSystemRight();

		int res = XG_OK;

		if (flag == "A")
		{
			res = add();
		}
		else if (flag == "D")
		{
			res = remove();
		}
		else if (flag == "U")
		{
			res = update();
		}

		return simpleResponse(res);
	}

	SmartBuffer content;

	if (app->getFileContent(app->getPath() + "app/tableview/pub/recordview.htm", content) <= 0) return clearResponse();

	string msg = stdx::replace(content.str(), "${recordoperhtml}", webx::GetScriptString(out.toString()));

	out.setContent(stdx::replace(msg, "${tabid}", "${config}"));
	out<<"<script>\n{\n\tlet vmdata = {\n\t\ttitle: [\'数据ID\', \'数据源ID\', \'数据表名\', \'数据名称\', \'数据说明\'],\n\t\tmodel: {id: \'\', dbid: \'\', name: \'\', title: \'\', remark: \'\'},\n\t\tstyle: [\n\t\t\t{size: 24, minlength: 1, maxlength: 32, filter: commonfilter.name},\n\t\t\t{size: 24, minlength: 1, maxlength: 32},\n\t\t\t{size: 24, minlength: 1, maxlength: 64, filter: commonfilter.name},\n\t\t\t{size: 24, minlength: 1, maxlength: 64},\n\t\t\t{size: 24, minlength: 0, maxlength: 256, type: \'textarea\'}\n\t\t]\n\t};\n\n\tfunction remove(item){\n\t\tif (item.enabled < 2) return showToast(\'当前记录不可删除\');\n\n\t\tshowConfirmMessage(\'是否要删除数据[\' + item.id + \']配置？\', \'删除选项\', function(flag){\n\t\t\tif (flag){\n\t\t\t\tgetHttpResult(\'/tableview/configview\', {id: item.id, flag: \'D\'}, function(data){\n\t\t\t\t\tif (data.code == XG_TIMEOUT){\n\t\t\t\t\t\tsessionTimeout();\n\t\t\t\t\t}\n\t\t\t\t\telse if (data.code < 0){\n\t\t\t\t\t\tshowToast(\'删除记录失败\');\n\t\t\t\t\t}\n\t\t\t\t\telse{\n\t\t\t\t\t\t$recordvmdata.recordview.reload(false);\n\n\t\t\t\t\t\tshowToast(\'删除记录成功\');\n\t\t\t\t\t}\n\t\t\t\t});\n\t\t\t}\n\t\t});\n\t}\n\n\tfunction update(item){\n\t\tif (item.enabled < 2) return showToast(\'当前记录不可修改\');\n\n\t\tvar data = Object.assign({}, vmdata);\n\n\t\tdata[\'model\'] = {id: item.id, dbid: item.dbid, name: item.name, title: item.title, remark: item.remark};\n\n\t\tvar elem = showConfirmDialog(data, \'修改配置\', function(flag){\n\t\t\tif (flag){\n\t\t\t\tvar param = Object.assign({flag: \'U\'}, data.model);\n\n\t\t\t\tgetHttpResult(\'/tableview/configview\', param, function(data){\n\t\t\t\t\tif (data.code ==  XG_TIMEOUT){\n\t\t\t\t\t\tsessionTimeout();\n\t\t\t\t\t}\n\t\t\t\t\telse if (data.code < 0){\n\t\t\t\t\t\tshowToast(\'修改配置失败\');\n\t\t\t\t\t}\n\t\t\t\t\telse{\n\t\t\t\t\t\t$recordvmdata.recordview.reload(false);\n\n\t\t\t\t\t\tshowToast(\'修改配置成功\');\n\t\t\t\t\t}\n\t\t\t\t});\n\t\t\t}\n\t\t});\n\n\t\t$(elem.remark).width($.pack(elem.id).attr(\'disabled\', true).width());\n\t}\n\n\tfunction columnView(item, elem){\n\t\tvar msg = getHttpResult(\'/tableview/columnview\', {tabid: item.id});\n\n\t\tshowConfirmMessage(msg, \'编辑字段\', function(flag){\n\t\t}, null, true, false);\n\n\t\t$(\'#XG_MSGBOX_OPTION_ROW_ID\').remove();\n\t}\n\n\t$(\'#AddRecordButton\').css(\'color\', \'#090\').click(function(){\n\t\tvar data = Object.assign({}, vmdata);\n\n\t\tdata[\'model\'] = {id: \'\', dbid: \'\', name: \'\', title: \'\', remark: \'\'};\n\n\t\tvar elem = showConfirmDialog(data, \'添加配置\', function(flag){\n\t\t\tif (flag){\n\t\t\t\tvar param = Object.assign({flag: \'A\'}, data.model);\n\n\t\t\t\tgetHttpResult(\'/tableview/configview\', param, function(data){\n\t\t\t\t\tif (data.code ==  XG_TIMEOUT){\n\t\t\t\t\t\tsessionTimeout();\n\t\t\t\t\t}\n\t\t\t\t\telse if (data.code < 0){\n\t\t\t\t\t\tshowToast(\'添加配置失败\');\n\t\t\t\t\t}\n\t\t\t\t\telse{\n\t\t\t\t\t\t$recordvmdata.recordview.reload(false);\n\n\t\t\t\t\t\tshowToast(\'添加配置成功\');\n\t\t\t\t\t}\n\t\t\t\t});\n\t\t\t}\n\t\t});\n\n\t\t$(elem.remark).width($.pack(elem.id).width());\n\t});\n\n\t$recordvmdata.button = [{\n\t\ttitle: \'字段\',\n\t\tcolor: \'#090\',\n\t\tclick: columnView\n\t}, {\n\t\ttitle: \'编辑\',\n\t\tcolor: \'#009\',\n\t\tclick: update\n\t}, {\n\t\ttitle: \'删除\',\n\t\tcolor: \'#C00\',\n\t\tclick: remove\n\t}];\n}\n</script>";


	return XG_OK;
}
HTTP_WEBAPP(ConfigView, CGI_PRIVATE, "/tableview/${filename}")