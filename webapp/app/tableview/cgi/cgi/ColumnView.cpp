#include <webx/menu.h>



class ColumnView : public webx::ProcessBase
{
protected:
	int process();
};

int ColumnView::process()
{
	param_string(flag);
	param_string(name);
	param_string(title);
	param_string(tabid);
	param_string(filter);
	param_string(remark);
	param_string(querytype);

	checkLogin();
	checkSystemRight();

	auto add = [&](){
		param_string(column);

		auto vec = stdx::split(column, ",");
		auto dbconn = webx::GetDBConnect();
		
		for (int i = 0; i < vec.size(); i++)
		{
			const string& field = vec[i];

			if (name == field)
			{
				dbconn->execute("INSERT INTO T_XG_TABCOLS(NAME,TABID,TITLE,FILTER,REMARK,QUERYTYPE,POSITION,ENABLED,STATETIME) VALUES(?,?,?,?,?,?,?,2,?)", name, tabid, title, filter, remark, querytype, i, DateTime::ToString());
			}
			else
			{
				dbconn->execute("UPDATE T_XG_TABCOLS SET position=? WHERE TABID=? AND NAME=? AND ENABLED>1", i, tabid, field);
			}
		}

	
		return XG_OK;
	};

	auto remove = [&](){
		return webx::GetDBConnect()->execute("DELETE FROM T_XG_TABCOLS WHERE TABID=? AND NAME=? AND ENABLED>1", tabid, name);
	};

	auto update = [&](){
		return webx::GetDBConnect()->execute("UPDATE T_XG_TABCOLS SET TITLE=?,REMARK=?,FILTER=?,QUERYTYPE=?,STATETIME=? WHERE TABID=? AND NAME=? AND ENABLED>1", title, remark, filter, querytype, DateTime::ToString(), tabid, name);
	};

	if (flag.length() > 0)
	{
		int res = XG_OK;

		if (flag == "A")
		{
			res = add();
		}
		else if (flag == "D")
		{
			res = remove();
		}
		else if (flag == "U")
		{
			res = update();
		}

		return simpleResponse(res);
	}
	out<<"<style>\n#RecordColumnDiv{\n\tmin-height: 60vh;\n}\n#RecordColumnDiv table{\n\tmin-width: 60vw;\n}\n#RecordColumnDiv table td{\n\tpadding: 5px 4px;\n\tborder: 1px dashed #CCC;\n}\n#RecordColumnDiv table button{\n\tcolor: #090;\n\tmargin: 0px 3px;\n\tpadding: 1px 2px;\n}\n#RecordColumnDiv table tr:first-child{\n\tbackground: rgba(80, 0, 80, 0.5);\n}\n#RecordColumnDiv table tr:first-child td{\n\tfont-size: 0.95rem;\n\tfont-weight: bold;\n}\n</style>\n\n<div id=\'RecordColumnDiv\'>\n\t<table>\n\t\t<tr>\n\t\t\t<td>字段名</td>\n\t\t\t<td>字段标题</td>\n\t\t\t<td>查询类型</td>\n\t\t\t<td>过滤条件</td>\n\t\t\t<td>字段说明</td>\n\t\t\t<td><button @click=\'addColumn(-1)\' class=\'TextButton\'>添加</button></td>\n\t\t</tr>\n\t\t<tr v-for=\'(item,index) in colist\'>\n\t\t\t<td>{{item.name}}</td>\n\t\t\t<td>{{item.title}}</td>\n\t\t\t<td>{{item.querytype}}</td>\n\t\t\t<td>{{item.filter}}</td>\n\t\t\t<td>{{item.remark}}</td>\n\t\t\t<td>\n\t\t\t\t<button @click=\'addColumn(index)\' class=\'TextButton\'>添加</button>\n\t\t\t\t<button @click=\'updateColumn(item)\' class=\'TextButton\' style=\'color:#009\'>编辑</button>\n\t\t\t\t<button @click=\'removeColumn(item)\' class=\'TextButton\' style=\'color:#C00\'>删除</button>\n\t\t\t</td>\n\t\t</tr>\n\t</table>\n</div>\n\n<script>\n{\n\tlet querytypelist = [\'不用查询\', \'完全匹配\', \'前缀匹配\', \'模糊匹配\', \'日期范围\', \'时间范围\'];\n\n\tlet vmdata = {\n\t\tcolist: [],\n\t\tcodata: {\n\t\t\ttitle: [\'查询条件\', \'字段名\', \'字段标题\', \'过滤条件\', \'字段说明\'],\n\t\t\tmodel: {querytype: \'不用查询\', name: \'\', title: \'\', filter: \'\', remark: \'\'},\n\t\t\tstyle: [\n\t\t\t\t{select: querytypelist},\n\t\t\t\t{size: 24, minlength: 1, maxlength: 32, filter: commonfilter.name},\n\t\t\t\t{size: 24, minlength: 1, maxlength: 32},\n\t\t\t\t{size: 24, minlength: 0, maxlength: 256, type: \'textarea\'},\n\t\t\t\t{size: 24, minlength: 0, maxlength: 256, type: \'textarea\'}\n\t\t\t]\n\t\t}\n\t}\n\n\tgetVue(\'RecordColumnDiv\', vmdata);\n\n\tfunction loadColumnList(){\n\t\tgetHttpResult(\'/tableview/getcolumnlist\', {tabid: \'";
	out<<(tabid);
	out<<"\'}, function(data){\n\t\t\tif (data.code > 0){\n\t\t\t\tfor (var i = 0; i < data.list.length; i++){\n\t\t\t\t\tdata.list[i].querytype = querytypelist[data.list[i].querytype];\n\t\t\t\t}\n\t\t\t\tvmdata.colist = data.list;\n\t\t\t}\n\t\t\telse{\n\t\t\t\tvmdata.colist = [];\n\t\t\t}\n\t\t});\n\t}\n\n\tfunction getQuerytype(text){\n\t\tfor (var i = 0; i < querytypelist.length; i++){\n\t\t\tif (text == querytypelist[i]) return i;\n\t\t}\n\t\treturn 0;\n\t}\n\n\tfunction addColumn(index){\n\t\tvar data = Object.assign({}, vmdata.codata);\n\n\t\tdata[\'model\'] = {querytype: \'不用查询\', name: \'\', title: \'\', filter: \'\', remark: \'\'};\n\n\t\tvar elem = showToastDialog(data, \'添加字段\', function(flag){\n\t\t\tif (flag){\n\t\t\t\tvar column = \'\';\n\t\t\t\tvar colist = vmdata.colist;\n\n\t\t\t\tif (index < 0) column += \',\' + data.model.name;\n\n\t\t\t\tif (colist.length > 0){\n\t\t\t\t\tfor (var i = 0; i < colist.length; i++){\n\t\t\t\t\t\tcolumn += \',\' + colist[i].name;\n\t\t\t\t\t\tif (i == index) column += \',\' + data.model.name;\n\t\t\t\t\t}\n\t\t\t\t\tcolumn = column.substring(1);\n\t\t\t\t}\n\n\t\t\t\tvar param = Object.assign({flag: \'A\', column: column, tabid: \'";
	out<<(tabid);
	out<<"\'}, data.model);\n\n\t\t\t\tparam.querytype = getQuerytype(param.querytype);\n\n\t\t\t\tgetHttpResult(\'/tableview/columnview\', param, function(data){\n\t\t\t\t\tif (data.code ==  XG_TIMEOUT){\n\t\t\t\t\t\tsessionTimeout();\n\t\t\t\t\t}\n\t\t\t\t\telse if (data.code < 0){\n\t\t\t\t\t\tshowToast(\'添加字段失败\');\n\t\t\t\t\t}\n\t\t\t\t\telse{\n\t\t\t\t\t\tloadColumnList();\n\n\t\t\t\t\t\tshowToast(\'添加字段成功\');\n\t\t\t\t\t}\n\t\t\t\t});\n\t\t\t}\n\t\t});\n\n\t\tvar cx = $(elem.name).width();\n\t\t$(elem.filter).width(cx);\n\t\t$(elem.remark).width(cx);\n\t}\n\n\tfunction updateColumn(item){\n\t\tvar data = Object.assign({}, vmdata.codata);\n\n\t\tdata[\'model\'] = {querytype: item.querytype, name: item.name, title: item.title, filter: item.filter, remark: item.remark};\n\n\t\tvar elem = showToastDialog(data, \'添加字段\', function(flag){\n\t\t\tif (flag){\n\t\t\t\tvar param = Object.assign({flag: \'U\', tabid: \'";
	out<<(tabid);
	out<<"\'}, data.model);\n\n\t\t\t\tparam.querytype = getQuerytype(param.querytype);\n\n\t\t\t\tgetHttpResult(\'/tableview/columnview\', param, function(data){\n\t\t\t\t\tif (data.code ==  XG_TIMEOUT){\n\t\t\t\t\t\tsessionTimeout();\n\t\t\t\t\t}\n\t\t\t\t\telse if (data.code < 0){\n\t\t\t\t\t\tshowToast(\'修改字段失败\');\n\t\t\t\t\t}\n\t\t\t\t\telse{\n\t\t\t\t\t\tloadColumnList();\n\n\t\t\t\t\t\tshowToast(\'修改字段成功\');\n\t\t\t\t\t}\n\t\t\t\t});\n\t\t\t}\n\t\t});\n\n\t\tvar cx = $(elem.name).attr(\'disabled\', true).width();\n\t\t$(elem.filter).width(cx);\n\t\t$(elem.remark).width(cx);\n\t}\n\n\tfunction removeColumn(item){\n\t\tshowToastDialog(\'是否决定删除字段[\' + item.title + \']？\', \'删除字段\', function(flag){\n\t\t\tif (flag){\n\t\t\t\tgetHttpResult(\'/tableview/columnview\', {flag: \'D\', name: item.name, tabid: \'";
	out<<(tabid);
	out<<"\'}, function(data){\n\t\t\t\t\tif (data.code ==  XG_TIMEOUT){\n\t\t\t\t\t\tsessionTimeout();\n\t\t\t\t\t}\n\t\t\t\t\telse if (data.code < 0){\n\t\t\t\t\t\tshowToast(\'删除字段失败\');\n\t\t\t\t\t}\n\t\t\t\t\telse{\n\t\t\t\t\t\tloadColumnList();\n\n\t\t\t\t\t\tshowToast(\'删除字段成功\');\n\t\t\t\t\t}\n\t\t\t\t});\n\t\t\t}\n\t\t});\n\t}\n\n\tloadColumnList();\n}\n</script>";


	return XG_OK;
}
HTTP_WEBAPP(ColumnView, CGI_PRIVATE, "/tableview/${filename}")