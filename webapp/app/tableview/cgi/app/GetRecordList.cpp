#include <webx/menu.h>

class GetRecordList : public webx::ProcessBase
{
	JsonEntity(Column)
	{
		rstring(dbid);
		rstring(name);
		rint(querytype);
	};

protected:
	int process();
	int getMetaData(Column& tab, vector<Column>& colist);
};

int GetRecordList::process()
{
	Column tab;
	string cols;
	string cond;
	vector<Column> colist;
	vector<DBData*> condlist;
	vector<sp<DBString>> condvec;

	if (getMetaData(tab, colist) <= 0) return simpleResponse(XG_PARAMERR);

	for (const Column& item : colist)
	{
		cols += "," + item.name;

		if (item.querytype <= 0) continue;

		string key = stdx::tolower(item.name);
		string tmp = request->getParameter(key);

		if (tmp.length() > 0)
		{
			if (item.querytype == 1)
			{
				cond += " AND " + item.name + "=?";
				condvec.push_back(newsp<DBString>(tmp));
			}
			else if (item.querytype == 2)
			{
				webx::CheckFileName(tmp);

				cond += " AND " + item.name + " LIKE '" + tmp + "%'";
			}
			else if (item.querytype == 3)
			{
				webx::CheckFileName(tmp);

				cond += " AND " + item.name + " LIKE '%" + tmp + "%'";
			}
			else
			{
				vector<string> vec = stdx::split(tmp, ",");

				if (vec.size() >= 1)
				{
					if (vec[0].length() > 0)
					{
						cond += " AND " + item.name + ">=?";
						condvec.push_back(newsp<DBString>(vec[0]));
					}

					if (vec.size() >= 2 && vec[1].length() > 0)
					{
						string endtime = vec[1];

						if (item.querytype == 4 || endtime.length() == 10) endtime += " 23:59:59";

						cond += " AND " + item.name + "<=?";
						condvec.push_back(newsp<DBString>(endtime));
					}
				}
			}
		}
	}

	sp<DBConnect> dbconn;

	if (tab.dbid == "${user}")
	{
		checkLogin();

		dbconn = webx::GetDBConnect(dbid);	
	}
	else
	{
		if (tab.dbid == "${system}") tab.dbid.clear();

		dbconn = webx::GetDBConnect(tab.dbid);
	}

	for (auto item : condvec) condlist.push_back(item.get());

	param_int(pagenum);
	param_int(pagesize);
	param_int(sortindex);

	if (pagenum < 0) pagenum = 0;
	if (pagesize < 1) pagesize = 10;
	if (pagesize > 100) pagesize = 100;

	if (tab.dbid.empty() && tab.name == "T_XG_TABETC")
	{
		cond = "FROM " + tab.name + " WHERE ENABLED>0" + cond;
	}
	else
	{
		cond = "FROM " + tab.name + " WHERE 1=1" + cond;
	}

	cols = cols.substr(1);

	int res = 0;
	int num = 0;
	string sqlcmd = "SELECT " + cols + " ";

	if (dbconn->select(num, "SELECT COUNT(*) " + cond, condlist) < 0) return simpleResponse(XG_PARAMERR);

	if (sortindex >= 0)
	{
		param_int(sortflag);

		string flag = sortflag > 1 ? " DESC" : " ASC";

		if (sortindex >= colist.size()) return simpleResponse(XG_PARAMERR);

		sqlcmd += cond + " ORDER BY " + colist[sortindex].name + flag + webx::GetLimitString(dbconn.get(), pagesize, pagenum);
	}
	else
	{
		sqlcmd += cond + webx::GetLimitString(dbconn.get(), pagesize, pagenum);
	}

	res = webx::PackJson(dbconn->query(sqlcmd, condlist), "list", json);

	json["count"] = num;
	json["code"] = res;
	out << json;

	return XG_OK;
}

int GetRecordList::getMetaData(Column& tab, vector<Column>& colist)
{
	param_string(tabid);

	if (tabid == "${config}")
	{
		checkLogin();
		checkSystemRight();
	}

	sp<DBConnect> dbconn = webx::GetDBConnect();

	dbconn->select(tab, "SELECT * FROM T_XG_TABETC WHERE ID=?", tabid);

	if (tab.name.empty()) return XG_PARAMERR;

	dbconn->selectList(colist, "SELECT * FROM T_XG_TABCOLS WHERE TABID=? AND ENABLED>0 ORDER BY POSITION ASC", tabid);

	if (colist.empty()) return XG_PARAMERR;

	return colist.size();
}

HTTP_WEBAPP(GetRecordList, CGI_PUBLIC, "/tableview/${filename}")