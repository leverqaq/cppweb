#include <webx/menu.h>

namespace
{
	JsonEntity(Column)
	{
	public:
		rstring(name);
		rstring(title);
		rstring(filter);
		rstring(remark);
		rint(enabled);
		rint(position);
		rint(querytype);
	};

	JsonEntity(Request)
	{
	public:
		rstring(tabid);
	};

	JsonEntity(Response)
	{
	public:
		rint(code);
		rarray(Column, list);
	};

	void dowork(Request& req, Response& rsp)
	{
		vector<Column> vec;

		webx::GetDBConnect()->selectList(vec, "SELECT * FROM T_XG_TABCOLS WHERE TABID=? AND ENABLED>0 ORDER BY POSITION ASC", req.tabid);

		for (Column item : vec)
		{
			rsp.list.add(newsp<Column>(item));
		}

		rsp.code = vec.size();
	}

	HTTP_WEBAPI(CGI_PUBLIC, "/tableview/${filename}", {
		Request req;
		Response rsp;

		parse(req);

		dowork(req, rsp);

		out << rsp.toString();

		return XG_OK;
	})
}